<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Test\Unit;

use Grifix\Acl\Domain\User\Email\UserEmail;
use Grifix\Acl\Domain\User\Email\UserEmailInfrastructureInterface;
use Grifix\Acl\Domain\User\Password\Password;
use Grifix\Acl\Domain\User\Password\PasswordInfrastructureInterface;
use Grifix\Acl\Domain\User\User;
use Grifix\Acl\Domain\User\UserInfrastructureInterface;
use Grifix\Kit\Test\Unit\AbstractTest;
use Grifix\Kit\Type\Email;
use Grifix\Kit\Type\IpAddress;
use Mockery as m;

/**
 * Class UserTest
 *
 * @category Grifix
 * @package  Grifix\AclService\Test\Unit
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class UserTest extends AbstractTest
{
    /**
     * Test if user with activation date in past can sign in
     *
     * @return void
     *
     * @throws \Throwable
     */
    public function testSingInCanBeActiveUser()
    {
        $userInfrastructure = m::mock(UserInfrastructureInterface::class);
        $userEmailInfrastructure = m::mock(UserEmailInfrastructureInterface::class);
        $passwordInfrastructure = m::mock(PasswordInfrastructureInterface::class);
        $userInfrastructure->shouldReceive('getDefaultTimezone')->andReturn(date_default_timezone_get());
        $userInfrastructure->shouldReceive('publishEvent');
        $userInfrastructure->shouldReceive('roleExists')->andReturn(true);
        $userEmailInfrastructure->shouldReceive('emailExists')->andReturn(false);

        $passwordInfrastructure->shouldReceive('validatePassword')->andReturn(true);
        $passwordInfrastructure->shouldReceive('hashPassword')->andReturn('password_hash');
        $passwordInfrastructure->shouldReceive('verifyPassword')->andReturn(true);
        /**@var $userInfrastructure UserInfrastructureInterface */
        /**@var $userEmailInfrastructure UserEmailInfrastructureInterface */
        $user = new User(
            $userInfrastructure,
            '1',
            new UserEmail($userEmailInfrastructure, new Email('user@example.com'), true),
            new Password($passwordInfrastructure, 'test'),
            null
        );
        $user->blockUntil(new \DateTime('01.01.2001'));
        $user->signIn('test', new IpAddress('127.0.0.1'), 'test');
    }
}
