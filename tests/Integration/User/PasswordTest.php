<?php
declare(strict_types=1);

namespace Grifix\Acl\Test\Integration\User;

use Grifix\Acl\Application\Command\ChangePassword\ChangePasswordCommand;
use Grifix\Acl\Application\Command\SingIn\SignInCommand;
use Grifix\Acl\Application\Command\SingOut\SignOutCommand;
use Grifix\Acl\Domain\User\UserFactoryInterface;
use Grifix\Acl\Domain\User\UserRepositoryInterface;
use Grifix\Kit\Cqrs\Command\CommandBusInterface;
use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;
use Grifix\Kit\Test\Integration\AbstractTest;
use Grifix\Kit\Type\Email;
use Grifix\Kit\Type\IpAddress;

/**
 * Class PasswordTest
 * @package Grifix\Acl\Test\Integration\User
 */
class PasswordTest extends AbstractTest
{
    public function testChangePassword()
    {
        $oldPassword = '3e\'%^.##b3gw`^H)';
        $newPassword = 'BJ:K<b&3B@xx7\"Z';
        $user = $this->getShared(UserFactoryInterface::class)->createUser(
            '1',
            new Email('user@example.com'),
            $oldPassword,
            true
        );

        $user->signIn($oldPassword, new IpAddress('192.168.0.1'), 'session_id');
        $user->signOut();
        $user->changePassword($oldPassword, $newPassword, $newPassword);
        $user->signIn($newPassword, new IpAddress('192.168.0.1'), 'session_id');
        self::assertTrue(true);
    }

    public function testChangePassword2()
    {
        $commandBus = $this->getShared(CommandBusInterface::class);
        $userId = '12345678';
        $ipAddress = new IpAddress('192.168.0.1');
        $sessionId = 'session_id';
        $oldPassword = '3e\'%^.##b3gw`^H)';
        $newPassword = 'BJ:K<b&3B@xx7\"Z';
        $email = new Email('user@example.com');
        $user = $this->getShared(UserFactoryInterface::class)->createUser(
            $userId,
            $email,
            $oldPassword,
            true
        );
        $this->getShared(UserRepositoryInterface::class)->save($user);

        $commandBus->execute(new SignInCommand($email, $oldPassword, $ipAddress, $sessionId));
        $commandBus->execute(new SignOutCommand($sessionId));

        $commandBus->execute(new ChangePasswordCommand($userId, $oldPassword, $newPassword, $newPassword));
        $commandBus->execute(new SignInCommand($email, $newPassword, $ipAddress, $sessionId));
        self::assertTrue(true);
    }
}
