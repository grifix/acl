<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Test\Integration;

use Grifix\Acl\Application\Command\ResetPermissions\ResetPermissionsCommand;
use Grifix\Kit\Cqrs\Command\CommandBusInterface;
use Grifix\Kit\Test\Integration\AbstractTest;

/**
 * Class AclTest
 *
 * @category Grifix
 * @package  Grifix\Kit\Test\Integration\Conversion
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class AclTest extends AbstractTest
{
    /**
     * @return void
     * @throws \Throwable
     */
    public function testResetPermissions()
    {
        $this->getShared(CommandBusInterface::class)->execute(new ResetPermissionsCommand('user'));
        self::assertTrue(true);
    }
}
