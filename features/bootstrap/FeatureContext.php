<?php

namespace Grifix\Acl\Behat;

use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Behat\Context\Context;
use Grifix\Acl\Domain\User\Email\Exception\UserEmailAlreadyExistsException;
use Grifix\Acl\Domain\User\Event\EmailConfirmedEvent;
use Grifix\Acl\Domain\User\Event\PasswordWasResetEvent;
use Grifix\Acl\Domain\User\Event\UserCreatedEvent;
use Grifix\Acl\Domain\User\Exception\EmailIsNotConfirmedException;
use Grifix\Acl\Ui\Http\Action\Exception\UserIsBlockedException;
use Grifix\Acl\Ui\Http\Action\Exception\UserNotExistException;
use Grifix\Kit\Behat\WebUiContext;
use Grifix\Kit\Config\ConfigInterface;
use Grifix\Kit\EventBus\EventBusInterface;
use Grifix\Kit\Mailer\MailerInterface;
use PHPUnit_Framework_Assert as PhpUnit;
use Mockery as m;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends WebUiContext implements Context
{
    public $token;
    public $password;
    protected $debug = false;

    /**
     * @BeforeScenario
     *
     * @param BeforeScenarioScope $scope
     *
     * @return void
     */
    public function beforeScenario(BeforeScenarioScope $scope)
    {
        parent::beforeScenario($scope);
        $this->token = null;
        $this->password = null;

        $this->mockMailer();
        $eventBus = $this->getShared(EventBusInterface::class);
        $that = $this;
        $eventBus->listen(PasswordWasResetEvent::class, function (PasswordWasResetEvent $event) use ($that) {
            $that->password = $event->getNewPassword();
        });

        $eventBus->listen(
            EmailConfirmedEvent::class,
            function (EmailConfirmedEvent $event) use ($that) {
                $that->password = $event->getPassword();
                $that->token = $event->getToken();
            }
        );

        $eventBus->listen(
            UserCreatedEvent::class,
            function (UserCreatedEvent $event) use ($that) {
                $that->token = $event->getToken();
            }
        );
    }


    protected function mockMailer()
    {
        $mailer = m::mock($this->getShared(MailerInterface::class));
        $mailer->shouldReceive('send')->andReturnUsing(function () {
            return true;
        });
        $this->setShared(MailerInterface::class, $mailer);
    }

    /**
     * @Given I am a guest
     */
    public function assertGuest()
    {
        $this->assertSignedInUser('guest@grifix.net');
    }


    /**
     * @Given There is an non confirmed account with email :email
     * @When  I try to sign up with email :email
     *
     * @param string $email
     *
     * @return void
     * @throws \Throwable
     */
    public function signUp(string $email)
    {
        $this->post(
            '/grifix/common/action/grifix.acl.signUp/json',
            [
                'email' => $email,
            ]
        );
    }

    /**
     * @When I receive an email with confirmation link
     */
    public function assertTokenSent()
    {
        PhpUnit::assertNotNull($this->token);
    }

    /**
     * @When I confirm email :arg1
     *
     * @param string $email
     *
     * @return void
     * @throws \Throwable
     *
     */
    public function confirmEmail(string $email)
    {
        $this->lastResponse->getBody()->getContents();
        $this->post(
            '/grifix/common/action/grifix.acl.confirmEmail/json',
            [
                'email' => $email,
                'token' => $this->token
            ]
        );
    }

    /**
     * @Then I receive email with my password
     */
    public function assertPasswordSent()
    {
        PhpUnit::assertNotNull($this->password);
    }

    /**
     * @When I sign in with email :arg1 and received password
     *
     * @param string $email
     *
     * @return void
     * @throws \Throwable
     */
    public function signInWithStoredPassword(string $email)
    {
        $this->signIn($email, $this->password);
    }

    /**
     * @Then I get the error that user with that email already used
     */
    public function assertEmailAlreadyUsedException()
    {
        $response = $this->getResponseArray();
        PhpUnit::assertEquals(500, $this->lastResponse->getStatusCode());
        PhpUnit::assertEquals(UserEmailAlreadyExistsException::class, $response['class']);
    }

    /**
     * @Given There is an account with email :email and password :password
     *
     * @param string $email
     * @param string $password
     *
     * @return void
     * @throws \Throwable
     */
    public function createAndActivateUser(string $email, string $password)
    {
        $this->signUp($email);
        $this->confirmEmail($email);
        $this->signInWithStoredPassword($email);
        $this->changePassword($this->password, $password, $password);
        $this->signOut();
    }

    public function signOut()
    {
        $this->post(
            '/grifix/common/action/grifix.acl.signOut/json'
        );
    }

    /**
     * @param string $oldPassword
     * @param string $newPassword
     * @param string $newPasswordAgain
     *
     * @return void
     * @throws \Throwable
     */
    public function changePassword(string $oldPassword, string $newPassword, string $newPasswordAgain)
    {
        $this->post('/grifix/common/action/grifix.acl.changePassword/json', [
            'old_password' => $oldPassword,
            'new_password' => $newPassword,
            'new_password_again' => $newPasswordAgain,
        ]);
    }

    /**
     * @When I try to sign in with email :email and password :password
     *
     * @param string $email
     * @param string $password
     *
     * @return void
     * @throws \Throwable
     */
    public function signIn(string $email, string $password)
    {
        $this->post('/grifix/common/action/grifix.acl.signIn/json', [
            'email' => $email,
            'password' => $password,
        ]);
    }

    /**
     * @Then I am signed in as a :email
     *
     * @param $email
     *
     * @return void
     */
    public function assertSignedInUser($email)
    {
        $this->get('/grifix/common/action/grifix.acl.getSignedInUser/json');
        $this->assertSuccessResponse();
        $user = $this->getResponseArray();
        PhpUnit::assertEquals($email, $user['email']);
    }

    /**
     * @Then I get an error that user is not exists
     */
    public function assertUserNotExistsException()
    {
        $response = $this->getResponseArray();
        PhpUnit::assertEquals(422, $this->lastResponse->getStatusCode());
        PhpUnit::assertEquals(UserNotExistException::class, $response['class']);
    }

    /**
     * @Then I get the error that my email is not confirmed
     *
     * @return void
     */
    public function assertEmailNotConfirmedException()
    {
        $response = $this->getResponseArray();
        PhpUnit::assertEquals(500, $this->lastResponse->getStatusCode());
        PhpUnit::assertEquals(EmailIsNotConfirmedException::class, $response['class']);
    }

    /**
     * @When I try to sign in with email :email and password :password more times than maximum of sign in limit
     *
     * @param $email
     * @param $password
     *
     * @return void
     * @throws \Throwable
     */
    public function bruteForceAttack($email, $password)
    {
        $maxAttempts = $this->getShared(ConfigInterface::class)->get('grifix.acl.signIn.maxAttempts');
        for ($i = 0; $i < $maxAttempts + 1; $i++) {
            $this->error = null;
            $this->signIn($email, $password);
        }
    }

    /**
     * @Then I get an error than user is blocked
     */
    public function assertUserIsBlockedException()
    {
        $response = $this->getResponseArray();
        PhpUnit::assertEquals(422, $this->lastResponse->getStatusCode());
        PhpUnit::assertEquals(UserIsBlockedException::class, $response['class']);
    }
}
