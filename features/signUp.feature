Feature: Sign up
  Scenario: Normal sign up
    Given I am a guest
    When I try to sign up with email "user@example.com"
    And I receive an email with confirmation link
    And I confirm email "user@example.com"
    And I receive email with my password
    And I sign in with email "user@example.com" and received password
    Then I am signed in as a "user@example.com"
    
  Scenario: Email already used
    Given I am a guest
    And There is an non confirmed account with email "user@example.com"
    When I try to sign up with email "user@example.com"
    Then I get the error that user with that email already used
    
  