Feature: Sign in
  
  Scenario: Normal sign in
    Given I am a guest
    And There is an account with email "user@example.com" and password "QwYu12!f^Dic"
    When I try to sign in with email "user@example.com" and password "QwYu12!f^Dic"
    Then I am signed in as a "user@example.com"
  
  Scenario: Invalid password
    Given I am a guest
    And There is an account with email "user@example.com" and password "QwYu12!f^Dic"
    When I try to sign in with email "user@example.com" and password "xxxx"
    Then I get an error that user is not exists
  
  Scenario: BruteForce attack
    Given I am a guest
    And There is an account with email "user@example.com" and password "QwYu12!f^Dic"
    When I try to sign in with email "user@example.com" and password "xxxx" more times than maximum of sign in limit
    Then I get an error than user is blocked
  
  Scenario: User not exists
    Given I am a guest
    When I try to sign in with email "user@example.com" and password "xxxx"
    Then I get an error that user is not exists
    
  Scenario: Not confirmed email
    Given I am a guest
    And There is an non confirmed account with email "user@example.com"
    When I try to sign in with email "user@example.com" and password "QwYu12!f^Dic"
    Then I get the error that my email is not confirmed