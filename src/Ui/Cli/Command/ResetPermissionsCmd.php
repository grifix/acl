<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Ui\Cli\Command;

use Grifix\Acl\Application\Command\ResetPermissions\ResetPermissionsCommand;
use Grifix\Acl\Application\Contract\RoleFinder\RoleDto;
use Grifix\Acl\Application\Query\FindRoles\FindRolesQuery;
use Grifix\Kit\Cli\AbstractCommand;
use Grifix\Kit\Cqrs\Command\CommandBusInterface;
use Grifix\Kit\Cqrs\Query\QueryBusInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ResetPermissionsCmd extends AbstractCommand
{
    protected const OPTION_ROLE = 'role';

    /**
     * @var CommandBusInterface
     */
    protected $commandBus;

    /**
     * @var QueryBusInterface
     */
    protected $queryBus;


    protected function init()
    {
        parent::init();
        $this->commandBus = $this->getShared(CommandBusInterface::class);
        $this->queryBus = $this->getShared(QueryBusInterface::class);
    }

    protected function configure()
    {
        $this
            ->setName('acl:reset_permissions')
            ->setDescription('reset permissions for groups.')
            ->addOption(self::OPTION_ROLE, 'r', null, 'Role name');
    }


    /** @noinspection PhpMissingParentCallCommonInspection */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $role = $input->getOption(self::OPTION_ROLE);
        if ($role) {
            $this->commandBus->execute(new ResetPermissionsCommand($role));
        } else {
            /**@var $roles RoleDto[] */
            $roles = $this->queryBus->execute(new FindRolesQuery());
            foreach ($roles as $role) {
                $this->commandBus->execute(new ResetPermissionsCommand($role->getName()));
            }
        }
    }
}
