<?php
declare(strict_types=1);

namespace Grifix\Acl\Ui;

class Permission
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string[]
     */
    protected $roles = [];

    /**
     * @var string|null
     */
    protected $translation;

    /**
     * Permission constructor.
     * @param string $name
     * @param string[] $roles
     * @param null|string $translation
     */
    public function __construct(string $name, array $roles, ?string $translation = null)
    {
        $this->name = $name;
        $this->roles = $roles;
        $this->translation = $translation;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string[]
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @return null|string
     */
    public function getTranslation(): ?string
    {
        return $this->translation;
    }
}
