<?php
declare(strict_types=1);

namespace Grifix\Acl\Ui;

/**
 * Interface Acl
 * @package Grifix\Acl
 */
interface AclModulePermissions
{
    public const SIGN_IN = 'grifix.acl.singIn';
    public const SIGN_UP = 'grifix.acl.signUp';
    public const CHANGE_PASSWORD = 'grifix.acl.changePassword';
    public const CONFIRM_EMAIL = 'grifix.acl.confirmEmail';
    public const RESET_PERMISSION = 'grifix.acl.resetPermission';
    public const GET_SIGNED_IN_USER = 'grifix.acl.getSignedInUser';
    public const ADMIN_ROLES = 'grifix.acl.admin.roles';
    public const ADMIN_USERS = 'grifix.acl.admin.users';
    public const READ_USER_EMAIL = 'grifix.acl.readUserEmail';
    public const READ_USER_PERMISSIONS = 'grifix.acl.readUserPermissions';
    public const READ_USER_BRUTEFORCE_COUNTER = 'grifix.acl.readUserBruteForceCounter';
    public const READ_USER_LAST_IP = 'grifix.acl.readUserLastIp';
    public const READ_USER_TOKEN = 'grifix.acl.readUserToken';
    public const READ_USER_TIMEZONE = 'grifix.acl.readUserTimezone';
}
