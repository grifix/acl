<?php
declare(strict_types=1);

namespace Grifix\Acl\Ui\Service\UserProtector;

use Grifix\Acl\Application\Contract\UserFinder\UserDto;

interface UserProtectorInterface
{
    public function protectUser(UserDto $userDto);
}
