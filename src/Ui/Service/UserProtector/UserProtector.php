<?php
declare(strict_types=1);

namespace Grifix\Acl\Ui\Service\UserProtector;

use Grifix\Acl\Ui\AclModulePermissions;
use Grifix\Acl\Application\Contract\UserFinder\UserDto;
use Grifix\Common\Ui\Service\PropertyProtector\PropertyProtectorInterface;
use Grifix\Common\Ui\Service\PropertyProtector\ProtectionDto;

class UserProtector implements UserProtectorInterface
{
    /**
     * @var PropertyProtectorInterface
     */
    protected $propertyProtector;


    public function __construct(PropertyProtectorInterface $propertyProtector)
    {
        $this->propertyProtector = $propertyProtector;
    }

    public function protectUser(UserDto $userDto)
    {
        $this->propertyProtector->protectProperties($userDto, [
            new ProtectionDto('email', AclModulePermissions::READ_USER_EMAIL),
            new ProtectionDto('permissions', AclModulePermissions::READ_USER_PERMISSIONS),
            new ProtectionDto('bruteForceCounter', AclModulePermissions::READ_USER_BRUTEFORCE_COUNTER),
            new ProtectionDto('lastIp', AclModulePermissions::READ_USER_LAST_IP),
            new ProtectionDto('token', AclModulePermissions::READ_USER_TOKEN),
            new ProtectionDto('timeZone', AclModulePermissions::READ_USER_TIMEZONE)
        ]);
    }
}
