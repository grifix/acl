<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Ui\Http\Action\Exception;

use Grifix\Kit\Ui\AbstractHttpException;

/**
 * Class UserNotExistException
 *
 * @category Grifix
 * @package  Grifix\AclService\Ui\Http\Request\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class UserNotExistException extends AbstractHttpException
{
    /**
     * {@inheritdoc}
     */
    protected function createMessage(): string
    {
        return $this->translator->translate('grifix.acl.userNotExists');
    }
}
