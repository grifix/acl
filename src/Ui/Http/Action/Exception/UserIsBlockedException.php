<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Ui\Http\Action\Exception;

use Grifix\Acl\Domain\User\Exception\UserIsNotActiveException;
use Grifix\Acl\Domain\User\UserInterface;
use Grifix\Kit\Ui\AbstractHttpException;

/**
 * Class UserIsBlockedException
 *
 * @category Grifix
 * @package  Grifix\AclService\Ui\Http\Request\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class UserIsBlockedException extends AbstractHttpException
{
    /**
     * @return string
     */
    protected function createMessage(): string
    {
        
        if ($this->isPeriodBlockade()) {
            /**@var $previous UserIsNotActiveException */
            $previous = $this->getPrevious();
            
            return $this->translator->translate(
                'grifix.acl.userIsBlockedUntil',
                ['date' => $previous->getActivationDate()->format('c')]
            );
        } else {
            return $this->translator->translate('grifix.acl.userIsBlocked');
        }
    }
    
    /**
     * @return bool
     */
    protected function isPeriodBlockade(): bool
    {
        $previous = $this->getPrevious();
        
        return (
            $previous instanceof UserIsNotActiveException
            && $previous->getActivationDate()
            && $previous->getBlockingReason() != UserInterface::REASON_BRUTEFORCE_ATTACK
        );
    }
}
