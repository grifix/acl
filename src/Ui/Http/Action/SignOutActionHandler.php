<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Acl\Ui\Http\Action;

use Grifix\Acl\Application\Command\SingOut\SignOutCommand;
use Grifix\Acl\Application\Command\SingUp\SignUpCommand;
use Grifix\Acl\Application\Query\GetFreeUserId\GetFreeUserIdQuery;
use Grifix\Kit\Session\SessionInterface;
use Grifix\Kit\Type\Email;
use Grifix\Kit\Ui\Action\AbstractActionHandler;
use Grifix\Kit\Validation\Exception\ValidationException;
use Grifix\Kit\Validation\Field\EmailField;
use Grifix\Kit\Validation\ValidationFactoryInterface;
use Grifix\Kit\Validation\ValidationInterface;
use Grifix\Kit\Validation\Validator\NotEmptyValidator;

/**
 * Class SignOutRequestHandler
 *
 * @category Grifix
 * @package  Grifix\AclService\Ui\Http\Request
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class SignOutActionHandler extends AbstractActionHandler
{
    protected $method = self::METHOD_POST;
    
    /**
     * @param array $params
     *
     * @return array
     *
     * @throws \Throwable
     */
    public function handle(array $params = []): array
    {
        $this->executeCommand(new SignOutCommand($this->getShared(SessionInterface::class)->getId()));
        
        return [];
    }
}
