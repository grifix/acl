<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Acl\Ui\Http\Action;

use Grifix\Acl\Domain\User\Exception\UserNotExistsException;
use Grifix\Acl\Application\Command\SingIn\SignInCommand;
use Grifix\Acl\Domain\User\Exception\UserIsNotActiveException;
use Grifix\Acl\Domain\User\Password\Exception\WrongPasswordException;
use Grifix\Acl\Ui\Http\Action\Exception\UserIsBlockedException;
use Grifix\Acl\Ui\Http\Action\Exception\UserNotExistException;
use Grifix\Kit\Http\ServerInterface;
use Grifix\Kit\Intl\TranslatorInterface;
use Grifix\Kit\Session\SessionInterface;
use Grifix\Kit\Type\Email;
use Grifix\Kit\Type\IpAddress;
use Grifix\Kit\Ui\Action\AbstractActionHandler;
use Grifix\Kit\Validation\Field\EmailField;
use Grifix\Kit\Validation\ValidationFactoryInterface;
use Grifix\Kit\Validation\ValidationInterface;

/**
 * Class SignInRequestHandler
 *
 * @category Grifix
 * @package  Grifix\AclService\Ui\Http\Request
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class SignInActionHandler extends AbstractActionHandler
{
    
    const EMAIL = 'email';
    const PASSWORD = 'password';
    
    protected $method = self::METHOD_POST;
    
    /**
     * {@inheritdoc}
     */
    public function handle(array $params = []): array
    {
        $this->createValidation()->validateOrFail($params);
        $translator = $this->getShared(TranslatorInterface::class);
        try {
            $this->executeCommand(
                new SignInCommand(
                    new Email($params[self::EMAIL]),
                    $params[self::PASSWORD],
                    new IpAddress($this->getShared(ServerInterface::class)->getClientIp()),
                    $this->getShared(SessionInterface::class)->getId()
                )
            );
        } catch (WrongPasswordException | UserNotExistsException $e) {
            throw new UserNotExistException($translator, $e);
        } catch (UserIsNotActiveException $e) {
            throw new UserIsBlockedException($translator, $e);
        }
        
        
        return [self::PASSWORD => null];
    }
    
    /**
     * @return ValidationInterface
     *
     * @throws \Exception
     */
    protected function createValidation(): ValidationInterface
    {
        $validation = $this->getShared(ValidationFactoryInterface::class)
            ->createValidation()->setStrategy(ValidationInterface::STRATEGY_ALL);
        $validation->createField(self::EMAIL, EmailField::class)->setNotEmpty();
        $validation->createField(self::PASSWORD)->setNotEmpty();
        
        return $validation;
    }
}
