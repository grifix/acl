<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Ui\Http\Action;

use Grifix\Acl\Application\Command\ConfirmEmail\ConfirmEmailCommand;
use Grifix\Kit\Type\Email;
use Grifix\Kit\Ui\Action\AbstractActionHandler;
use Grifix\Kit\Validation\Field\EmailField;
use Grifix\Kit\Validation\ValidationFactoryInterface;
use Grifix\Kit\Validation\ValidationInterface;
use Grifix\Kit\Validation\Validator\NotEmptyValidator;

/**
 * Class ConfirmEmailRequestHandler
 *
 * @category Grifix
 * @package  Grifix\AclService\Ui\Http\Request
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ConfirmEmailActionHandler extends AbstractActionHandler
{
    const EMAIL = 'email';
    const TOKEN = 'token';
    
    protected $method = self::METHOD_POST;
    
    /**
     * @inheritdoc
     */
    public function handle(array $params = []): array
    {
        $this->createValidation()->validateOrFail($params);
        $this->executeCommand(new ConfirmEmailCommand(new Email($params[self::EMAIL]), $params[self::TOKEN]));
        return [];
    }
    
    /**
     * @return ValidationInterface
     * @throws \Exception
     */
    protected function createValidation(): ValidationInterface
    {
        $validation = $this->getShared(ValidationFactoryInterface::class)->createValidation()
            ->setStrategy(ValidationInterface::STRATEGY_ALL);
        $validation->createField(self::EMAIL, EmailField::class)->setNotEmpty();
        $validation->createField(self::TOKEN)->setNotEmpty();
        
        return $validation;
    }
}
