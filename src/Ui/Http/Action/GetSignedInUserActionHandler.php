<?php


namespace Grifix\Acl\Ui\Http\Action;

use Grifix\Acl\Application\Query\GetSignedInUser\GetSignedInUserQuery;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Session\SessionInterface;
use Grifix\Kit\Ui\Action\AbstractActionHandler;

/**
 * Class GetSignedInUserHandler
 * @package Grifix\Acl\Ui\Http\Request
 */
class GetSignedInUserActionHandler extends AbstractActionHandler
{
    protected $method = self::METHOD_GET;

    /**
     * {@inheritdoc}
     */
    public function handle(array $params = []): array
    {
        $result = $this->executeQuery(new GetSignedInUserQuery($this->getShared(SessionInterface::class)->getId()));
        return $this->getShared(ArrayHelperInterface::class)->toArray($result);
    }
}
