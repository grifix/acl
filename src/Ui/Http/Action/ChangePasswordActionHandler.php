<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Ui\Http\Action;

use Grifix\Acl\Application\Command\ChangePassword\ChangePasswordCommand;
use Grifix\Acl\Application\Query\GetSignedInUser\GetSignedInUserQuery;
use Grifix\Acl\Application\Contract\UserFinder\UserDto;
use Grifix\Kit\Session\SessionInterface;
use Grifix\Kit\Ui\Action\AbstractActionHandler;
use Grifix\Kit\Validation\ValidationFactoryInterface;
use Grifix\Kit\Validation\ValidationInterface;

/**
 * Class ChangePasswordRequestHandler
 *
 * @category Grifix
 * @package  Grifix\AclService\Ui\Http\Request
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ChangePasswordActionHandler extends AbstractActionHandler
{
    const OLD_PASSWORD = 'old_password';
    const NEW_PASSWORD = 'new_password';
    const NEW_PASSWORD_AGAIN = 'new_password_again';
    
    protected $method = self::METHOD_POST;
    
    /**
     * {@inheritdoc}
     */
    public function handle(array $params = []): array
    {
        /**@var $user UserDto */
        $user = $this->executeQuery(new GetSignedInUserQuery($this->getShared(SessionInterface::class)->getId()));
        
        $this->createValidation()->validateOrFail($params);
        $this->executeCommand(new ChangePasswordCommand(
            $user->getId(),
            $params[self::OLD_PASSWORD],
            $params[self::NEW_PASSWORD],
            $params[self::NEW_PASSWORD_AGAIN]
        ));
        
        return [];
    }
    
    /**
     * @return ValidationInterface
     * @throws \Exception
     */
    protected function createValidation(): ValidationInterface
    {
        $validation = $this->getShared(ValidationFactoryInterface::class)->createValidation()
            ->setStrategy(ValidationInterface::STRATEGY_ALL);
        $validation->createField(self::OLD_PASSWORD)->setNotEmpty();
        $validation->createField(self::NEW_PASSWORD)->setNotEmpty();
        $validation->createField(self::NEW_PASSWORD_AGAIN)->setNotEmpty();
        
        return $validation;
    }
}
