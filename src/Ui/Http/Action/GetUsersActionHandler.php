<?php
declare(strict_types=1);

namespace Grifix\Acl\Ui\Http\Action;

use Grifix\Acl\Application\Contract\UserFinder\UserFilter;
use Grifix\Acl\Application\Query\FindUsers\FindUsersQuery;
use Grifix\Acl\Application\Query\FindUsers\FindUsersQueryResult;
use Grifix\Acl\Ui\Service\UserProtector\UserProtectorInterface;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Helper\OffsetCalculatorInterface;
use Grifix\Kit\Ui\Action\AbstractActionHandler;

/**
 * Class GetUsersRequestHandler
 * @package Grifix\Acl\Ui\Http\Request
 */
class GetUsersActionHandler extends AbstractActionHandler
{

    public const PARAM_PAGE = 'page';
    public const PARAM_ROWS = 'rows';

    protected $method = self::METHOD_GET;

    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;

    /**
     * @var OffsetCalculatorInterface
     */
    protected $offsetCalculator;

    /**
     * @var UserProtectorInterface;
     */
    protected $userProtector;


    protected function init()
    {
        $this->arrayHelper = $this->getShared(ArrayHelperInterface::class);
        $this->offsetCalculator = $this->getShared(OffsetCalculatorInterface::class);
        $this->userProtector = $this->getShared(UserProtectorInterface::class);
    }

    /**
     * {@inheritdoc}
     */
    public function handle(array $params = []): array
    {
        $this->init();
        $page = $this->arrayHelper->get($params, static::PARAM_PAGE);
        $rows = $this->arrayHelper->get($params, static::PARAM_ROWS);

        $page === null ?: $page = (int)$page;
        $rows === null ?: $rows = (int)$rows;

        $offset = $this->offsetCalculator->calculateOffset($page, $rows);

        /**@var $result FindUsersQueryResult */
        $result = $this->executeQuery(new FindUsersQuery(
            new UserFilter(),
            $offset,
            $rows,
            true
        ));
        foreach ($result->getUsers() as $user) {
            $this->userProtector->protectUser($user);
        }
        return ['result' => $result];
    }
}
