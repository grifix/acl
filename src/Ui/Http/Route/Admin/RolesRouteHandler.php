<?php
declare(strict_types=1);

namespace Grifix\Acl\Ui\Http\Route\Admin;

use Grifix\Kit\Http\ResponseInterface;
use Grifix\Kit\Http\ServerRequestInterface;
use Grifix\Kit\Route\Handler\AbstractRouteHandler;
use Grifix\Kit\Route\RouteInterface;
use Grifix\Kit\View\ViewFactoryInterface;

/**
 * Class RolesRouteHandler
 * @package Grifix\Acl\Ui\Http\Route\Admin
 */
class RolesRouteHandler extends AbstractRouteHandler
{
    public function handle(
        RouteInterface $route,
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        return $response->withContent(
            $this->getShared(ViewFactoryInterface::class)
                ->create('grifix.acl.{skin}.admin.role.tpl.index')
                ->render()
        );
    }
}
