<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Application\Query\GetFreeUserId;

use Grifix\Kit\Uuid\UuidGeneratorInterface;

/**
 * Class GetFreeUserIdQueryHandler
 *
 * @category Grifix
 * @package  Grifix\AclService\Application\Query
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class GetFreeUserIdQueryHandler
{

    /**
     * @var UuidGeneratorInterface
     */
    protected $uuidGenerator;

    /**
     * GetFreeUserIdQueryHandler constructor.
     * @param UuidGeneratorInterface $uuidGenerator
     */
    public function __construct(UuidGeneratorInterface $uuidGenerator)
    {
        $this->uuidGenerator = $uuidGenerator;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(GetFreeUserIdQuery $query): string
    {
        return $this->uuidGenerator->generate();
    }
}
