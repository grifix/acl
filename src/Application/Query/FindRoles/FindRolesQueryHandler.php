<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Application\Query\FindRoles;

use Grifix\Acl\Application\Contract\RoleFinder\RoleFinderInterface;

/**
 * Class FindRolesQueryHandler
 *
 * @category Grifix
 * @package  Grifix\Acl\Application\Query
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class FindRolesQueryHandler
{
    /**
     * @var RoleFinderInterface
     */
    protected $roleFinder;

    /**
     * FindRolesQueryHandler constructor.
     * @param RoleFinderInterface $roleFinder
     */
    public function __construct(RoleFinderInterface $roleFinder)
    {
        $this->roleFinder = $roleFinder;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(FindRolesQuery $query)
    {
        return $this->roleFinder->find($query->getFilter());
    }
}
