<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Acl\Application\Query\FindRoles;

use Grifix\Acl\Application\Contract\RoleFinder\RoleFilter;

/**
 * Class FindRolesQuery
 *
 * @category Grifix
 * @package  Grifix\Acl\Application\Query
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class FindRolesQuery
{
    /**
     * @var RoleFilter
     */
    private $filter;

    /**
     * FindRolesQuery constructor.
     * @param RoleFilter $filter
     */
    public function __construct(?RoleFilter $filter = null)
    {
        $this->filter = $filter ?? new RoleFilter();
    }

    /**
     * @return RoleFilter
     */
    public function getFilter(): RoleFilter
    {
        return $this->filter;
    }
}
