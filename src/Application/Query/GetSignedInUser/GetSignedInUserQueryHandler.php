<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Acl\Application\Query\GetSignedInUser;

use Grifix\Acl\Application\Contract\UserFinder\UserFilter;
use Grifix\Acl\Application\Contract\UserFinder\UserDto;
use Grifix\Acl\Application\Contract\UserFinder\UserFinderInterface;
use Grifix\Kit\Collection\CollectionInterface;

/**
 * Class GetSignedInUserQueryHandler
 *
 * @category Grifix
 * @package  Grifix\AclService\Application\Query
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class GetSignedInUserQueryHandler
{

    /**
     * @var UserFinderInterface
     */
    protected $userFinder;

    /**
     * GetSignedInUserQueryHandler constructor.
     * @param UserFinderInterface $userFinder
     */
    public function __construct(UserFinderInterface $userFinder)
    {
        $this->userFinder = $userFinder;
    }

    /**
     * @param GetSignedInUserQuery $query
     *
     * @return UserDto
     */
    public function __invoke(GetSignedInUserQuery $query): UserDto
    {
        /**@var $result CollectionInterface*/
        $result = $this->userFinder->find((new UserFilter())->setSessionId($query->getSessionId()));
        
        if (!$result->first()) {
            $result = $this->userFinder->find((new UserFilter())->setIsGuest(true));
        }
        
        return $result->first();
    }
}
