<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Application\Query\FindUsers;

use Grifix\Acl\Application\Contract\UserFinder\UserFilter;

/**
 * Class FindUsersQuery
 *
 * @category Grifix
 * @package  Grifix\AclService\Application\Query
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class FindUsersQuery
{

    /**
     * @var UserFilter
     */
    protected $filter;

    /**
     * @var int|null
     */
    protected $offset;

    /**
     * @var int|null
     */
    protected $limit;

    /**
     * @var bool
     */
    protected $countTotal;


    public function __construct(
        ?UserFilter $filter = null,
        ?int $offset = null,
        ?int $limit = null,
        bool $countTotal = false
    ) {
        $this->filter = $filter ?? new UserFilter();
        $this->offset = $offset;
        $this->limit = $limit;
        $this->countTotal = $countTotal;
    }

    /**
     * @return UserFilter|null
     */
    public function getFilter(): UserFilter
    {
        return $this->filter;
    }

    /**
     * @return int|null
     */
    public function getOffset(): ?int
    {
        return $this->offset;
    }

    /**
     * @return int|null
     */
    public function getLimit(): ?int
    {
        return $this->limit;
    }

    /**
     * @return bool
     */
    public function getCountTotal(): bool
    {
        return $this->countTotal;
    }
}
