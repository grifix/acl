<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Application\Query\FindUsers;

use Grifix\Acl\Application\Contract\UserFinder\UserFinderInterface;
use Grifix\Kit\Collection\CollectionInterface;

/**
 * Class FindUsersQueryHandler
 *
 * @category Grifix
 * @package  Grifix\AclService\Application\Query
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class FindUsersQueryHandler
{
    protected $userFinder;

    public function __construct(UserFinderInterface $userFinder)
    {
        $this->userFinder = $userFinder;
    }

    public function __invoke(FindUsersQuery $query): FindUsersQueryResult
    {
        $users = $this->userFinder->find($query->getFilter(), $query->getOffset(), $query->getLimit());
        $total = null;
        if ($query->getCountTotal()) {
            $total = $this->userFinder->count($query->getFilter());
        }
        return new FindUsersQueryResult($users, $total);
    }
}
