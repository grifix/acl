<?php
declare(strict_types=1);

namespace Grifix\Acl\Application\Query\FindUsers;

use Grifix\Acl\Application\Contract\UserFinder\UserDto;
use Grifix\Kit\Collection\CollectionInterface;

class FindUsersQueryResult
{

    /**
     * @var UserDto[]
     */
    protected $users;

    /**
     * @var int|null
     */
    protected $total;

    /**
     * FindUsersQueryResult constructor.
     * @param CollectionInterface|UserDto[] $users
     * @param int|null $total
     */
    public function __construct(CollectionInterface $users, ?int $total)
    {
        $this->users = $users;
        $this->total = $total;
    }

    /**
     * @return UserDto[] | CollectionInterface
     */
    public function getUsers(): CollectionInterface
    {
        return $this->users;
    }

    /**
     * @return int|null
     */
    public function getTotal(): ?int
    {
        return $this->total;
    }
}
