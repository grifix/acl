<?php
declare(strict_types=1);

namespace Grifix\Acl\Application\Query\GeneratePassword;

use Grifix\Acl\Application\Query\GeneratePassword\GeneratePasswordQuery;
use Grifix\Acl\Infrastructure\User\Password\PasswordGeneratorInterface;

/**
 * Class GeneratePasswordQueryHandler
 * @package Grifix\Acl\Application\Query
 */
class GeneratePasswordQueryHandler
{

    /**
     * @var PasswordGeneratorInterface
     */
    protected $passwordGenerator;

    /**
     * GeneratePasswordQueryHandler constructor.
     * @param PasswordGeneratorInterface $passwordGenerator
     */
    public function __construct(PasswordGeneratorInterface $passwordGenerator)
    {
        $this->passwordGenerator = $passwordGenerator;
    }


    /**
     * {@inheritdoc}
     */
    public function __invoke(GeneratePasswordQuery $query)
    {
        return $this->passwordGenerator->generatePassword();
    }
}
