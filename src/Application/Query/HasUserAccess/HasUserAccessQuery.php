<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Acl\Application\Query\HasUserAccess;

/**
 * Class HasSignedUserAccessQuery
 *
 * @category Grifix
 * @package  Grifix\AclService\Application\Query
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class HasUserAccessQuery
{
    /**
     * @var string
     */
    protected $userId;
    
    /**
     * @var string
     */
    protected $permission;
    
    /**
     * HasSignedUserAccessQuery constructor.
     *
     * @param int|string $userId
     * @param string     $permission
     */
    public function __construct(string $userId, string $permission)
    {
        $this->userId = $userId;
        $this->permission = $permission;
    }
    
    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }
    
    /**
     * @return string
     */
    public function getPermission(): string
    {
        return $this->permission;
    }
}
