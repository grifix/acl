<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Application\Query\HasUserAccess;

use Grifix\Acl\Application\Contract\UserFinder\UserDto;
use Grifix\Acl\Application\Contract\UserFinder\UserFilter;
use Grifix\Acl\Application\Contract\UserFinder\UserFinderInterface;
use Grifix\Acl\Application\Query\HasUserAccess\Exception\UserNotFoundException;

/**
 * Class HasSignedUserAccessQueryHandler
 *
 * @category Grifix
 * @package  Grifix\AclService\Application\Query
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class HasUserAccessQueryHandler
{

    /**
     * @var UserFinderInterface
     */
    protected $userFinder;

    /**
     * HasUserAccessQueryHandler constructor.
     * @param UserFinderInterface $userFinder
     */
    public function __construct(UserFinderInterface $userFinder)
    {
        $this->userFinder = $userFinder;
    }

    /**
     * @param HasUserAccessQuery $query
     * @return bool
     * @throws \Grifix\Kit\Db\Exception\StatementExecuteException
     */
    public function __invoke(HasUserAccessQuery $query): bool
    {
        $filter = (new UserFilter())->setId($query->getUserId())->withPermissions();

        /**@var $user UserDto */
        $user = $this->userFinder->find($filter)->first();
        if (!$user) {
            throw new UserNotFoundException();
        }
        return in_array($query->getPermission(), $user->getPermissions());
    }

}
