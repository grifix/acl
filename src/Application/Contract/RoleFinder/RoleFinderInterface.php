<?php
declare(strict_types=1);

namespace Grifix\Acl\Application\Contract\RoleFinder;

use Grifix\Kit\Collection\CollectionInterface;

/**
 * Interface RoleFinderInterface
 * @package Grifix\Acl\Application\Contract\RoleFinder
 */
interface RoleFinderInterface
{
    /**
     * @param RoleFilter $filter
     * @return CollectionInterface
     */
    public function find(RoleFilter $filter): CollectionInterface;
}
