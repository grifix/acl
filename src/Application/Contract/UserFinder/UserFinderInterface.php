<?php
declare(strict_types=1);

namespace Grifix\Acl\Application\Contract\UserFinder;

use Grifix\Kit\Collection\CollectionInterface;

interface UserFinderInterface
{
    public function find(UserFilter $filter, ?int $offset = null, ?int $limit = null): CollectionInterface;

    public function count(UserFilter $filter): int;
}
