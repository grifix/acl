<?php
declare(strict_types=1);

namespace Grifix\Acl\Application\Contract\UserFinder;

/**
 * Class Params
 * @package Grifix\Acl\Application\Contract\UserFinder
 */
class UserFilter
{
    /**
     * @var string|null
     */
    protected $id;

    /**
     * @var string|null
     */
    protected $sessionId;

    /**
     * @var bool
     */
    protected $withPermissions;

    /**
     * @var bool
     */
    protected $isGuest;

    /**
     * Params constructor.
     * @param null|string $id
     * @param null|string $sessionId
     * @param bool $withPermission
     * @param bool $isGuest
     */
    public function __construct(
        ?string $id = null,
        ?string $sessionId = null,
        bool $withPermission = false,
        bool $isGuest = false
    ) {
        $this->id = $id;
        $this->sessionId = $sessionId;
        $this->withPermissions = $withPermission;
        $this->isGuest = $isGuest;
    }

    /**
     * @return null|string
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param null|string $id
     * @return UserFilter
     */
    public function setId(?string $id): UserFilter
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getSessionId(): ?string
    {
        return $this->sessionId;
    }

    /**
     * @param null|string $sessionId
     * @return UserFilter
     */
    public function setSessionId(?string $sessionId): UserFilter
    {
        $this->sessionId = $sessionId;
        return $this;
    }

    /**
     * @return bool
     */
    public function isWithPermissions(): bool
    {
        return $this->withPermissions;
    }

    /**
     * @return UserFilter
     */
    public function withPermissions(): UserFilter
    {
        $this->withPermissions = true;
        return $this;
    }

    /**
     * @return bool
     */
    public function isGuest(): bool
    {
        return $this->isGuest;
    }

    /**
     * @param bool $isGuest
     * @return UserFilter
     */
    public function setIsGuest(bool $isGuest): UserFilter
    {
        $this->isGuest = $isGuest;
        return $this;
    }
}
