<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Application\Contract\UserFinder;

use Grifix\Kit\Type\IpAddress;
use Grifix\Kit\Type\IpAddressInterface;
use Grifix\Kit\Type\TimeZone;
use Grifix\Kit\Type\TimeZoneInterface;

/**
 * Class UserModel
 *
 * @category Grifix
 * @package  Grifix\AclService\Application\Dto
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class UserDto
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var \DateTimeInterface
     */
    protected $registrationDate;

    /**
     * @var bool
     */
    protected $isActive;

    /**
     * @var bool
     */
    protected $emailConfirmed;

    /**
     * @var int
     */
    protected $bruteForceCounter;

    /**
     * @var \DateTimeInterface|null
     */
    protected $lastSignInDate;

    /**
     * @var IpAddressInterface|null
     */
    protected $lastIp;

    /**
     * @var TimeZoneInterface|null
     */
    protected $timeZone;

    /**
     * @var null|string
     */
    protected $token;

    /**
     * @var bool
     */
    protected $isGuest;

    /**
     * @var array|null
     */
    protected $permissions;

    /**
     * UserDto constructor.
     * @param string $id
     * @param string $email
     * @param \DateTimeInterface $registrationDate
     * @param bool $isActive
     * @param bool $emailConfirmed
     * @param int $bruteForceCounter
     * @param bool $isGuest
     * @param \DateTimeInterface|null $lastSignDate
     * @param IpAddressInterface|null $lastIp
     * @param TimeZoneInterface|null $timeZone
     * @param string|null $token
     * @param array|null $permissions
     */
    public function __construct(
        string $id,
        string $email,
        \DateTimeInterface $registrationDate,
        bool $isActive,
        bool $emailConfirmed,
        int $bruteForceCounter,
        bool $isGuest,
        \DateTimeInterface $lastSignDate = null,
        IpAddressInterface $lastIp = null,
        TimeZoneInterface $timeZone = null,
        string $token = null,
        ?array $permissions = null
    ) {
        $this->id = $id;
        $this->email = $email;
        $this->registrationDate = $registrationDate;
        $this->isActive = $isActive;
        $this->emailConfirmed = $emailConfirmed;
        $this->bruteForceCounter = $bruteForceCounter;
        $this->lastSignInDate = $lastSignDate;
        $this->lastIp = $lastIp;
        $this->timeZone = $timeZone;
        $this->token = $token;
        $this->isGuest = $isGuest;
        $this->permissions = $permissions;
    }

    /**
     * @return int
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getRegistrationDate(): \DateTimeInterface
    {
        return $this->registrationDate;
    }

    /**
     * @return bool
     */
    public function isIsActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @return bool
     */
    public function isEmailConfirmed(): bool
    {
        return $this->emailConfirmed;
    }

    /**
     * @return int
     */
    public function isBruteForceCounter(): int
    {
        return $this->bruteForceCounter;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getLastSignInDate(): ?\DateTimeInterface
    {
        return $this->lastSignInDate;
    }

    /**
     * @return IpAddressInterface|null
     */
    public function getLastIp(): ?IpAddressInterface
    {
        return $this->lastIp;
    }

    /**
     * @return TimeZoneInterface|null
     */
    public function getTimeZone(): ?TimeZoneInterface
    {
        return $this->timeZone;
    }

    /**
     * @return string|null
     */
    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @return int
     */
    public function getBruteForceCounter(): int
    {
        return $this->bruteForceCounter;
    }

    /**
     * @return bool
     */
    public function getIsGuest(): bool
    {
        return $this->isGuest;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @return array|null
     */
    public function getPermissions(): ?array
    {
        return $this->permissions;
    }
}
