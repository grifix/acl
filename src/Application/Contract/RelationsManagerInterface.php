<?php
declare(strict_types=1);

namespace Grifix\Acl\Application\Contract;

/**
 * Class RoleSynchronizer
 * @package Grifix\Acl\Infrastructure\Role
 */
interface RelationsManagerInterface
{
    /**
     * @param string $userId
     * @param string $roleId
     * @throws \Grifix\Kit\Db\Exception\StatementExecuteException
     */
    public function removeRelation(string $userId, string $roleId);

    /**
     * @param string $userId
     * @param string $roleId
     * @throws \Grifix\Kit\Db\Exception\StatementExecuteException
     */
    public function createRelation(string $userId, string $roleId);
}
