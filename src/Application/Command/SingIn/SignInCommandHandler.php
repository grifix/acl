<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Application\Command\SingIn;

use Grifix\Acl\Domain\User\UserRepositoryInterface;

/**
 * Class SignInCommandHandler
 *
 * @category Grifix
 * @package  Grifix\AclService\Application\Command
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class SignInCommandHandler
{

    /**
     * @var UserRepositoryInterface
     */
    protected $userRepository;

    /**
     * SignInCommandHandler constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(SignInCommand $command): void
    {
        $user = $this->userRepository->findByEmail($command->getEmail());
        try {
            $user->signIn($command->getPassword(), $command->getIp(), $command->getSessionId());
        } catch (\Throwable $e) {
            throw $e;
        } finally {
            $this->userRepository->save($user);
        }
    }
}
