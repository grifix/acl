<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Application\Command\SingIn;

use Grifix\Kit\Type\EmailInterface;
use Grifix\Kit\Type\IpAddressInterface;

/**
 * Class SignInCommand
 *
 * @category Grifix
 * @package  Grifix\AclService\Application\Command
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class SignInCommand
{
    /**
     * @var EmailInterface
     */
    protected $email;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var IpAddressInterface
     */
    protected $ip;

    /**
     * @var string
     */
    protected $sessionId;

    /**
     * SignInCommand constructor.
     *
     * @param EmailInterface $email
     * @param string $password
     * @param IpAddressInterface $ip
     * @param string $sessionId
     */
    public function __construct(EmailInterface $email, string $password, IpAddressInterface $ip, string $sessionId)
    {
        $this->email = $email;
        $this->password = $password;
        $this->ip = $ip;
        $this->sessionId = $sessionId;
    }

    /**
     * @return EmailInterface
     */
    public function getEmail(): EmailInterface
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return IpAddressInterface
     */
    public function getIp(): IpAddressInterface
    {
        return $this->ip;
    }

    /**
     * @return string
     */
    public function getSessionId(): string
    {
        return $this->sessionId;
    }
}
