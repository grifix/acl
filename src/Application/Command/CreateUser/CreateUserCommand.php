<?php
declare(strict_types=1);

namespace Grifix\Acl\Application\Command\CreateUser;

use Grifix\Kit\Type\EmailInterface;

/**
 * Class CreateUserCommand
 * @package Grifix\Acl\Application\Command
 */
class CreateUserCommand
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var EmailInterface
     */
    protected $email;

    /**
     * @var ?string
     */
    protected $password;

    /**
     * @var string
     */
    protected $roleId;

    /**
     * CreateUserCommand constructor.
     * @param string $id
     * @param EmailInterface $email
     * @param string $roleId
     * @param string $password
     */
    public function __construct(
        string $id,
        EmailInterface $email,
        string $roleId,
        ?string $password = null
    ) {
        $this->id = $id;
        $this->email = $email;
        $this->password = $password;
        $this->roleId = $roleId;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return EmailInterface
     */
    public function getEmail(): EmailInterface
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getRoleId(): string
    {
        return $this->roleId;
    }
}
