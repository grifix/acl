<?php
declare(strict_types=1);

namespace Grifix\Acl\Application\Command\CreateUser;

use Grifix\Acl\Domain\User\UserFactoryInterface;
use Grifix\Acl\Domain\User\UserRepositoryInterface;

/**
 * Class CreateUserCommandHandler
 * @package Grifix\Acl\Application\Command
 */
class CreateUserCommandHandler
{

    /**
     * @var UserFactoryInterface
     */
    private $userFactory;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * CreateUserCommandHandler constructor.
     * @param UserFactoryInterface $userFactory
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(
        UserFactoryInterface $userFactory,
        UserRepositoryInterface $userRepository
    ) {
        $this->userRepository = $userRepository;
        $this->userFactory = $userFactory;
    }


    /**
     * @param CreateUserCommand $command
     */
    public function __invoke(CreateUserCommand $command): void
    {
        $user = $this->userFactory->createUser(
            $command->getId(),
            $command->getEmail(),
            $command->getPassword(),
            true
        );
        $user->assignRole($command->getRoleId());
        $this->userRepository->save($user);
    }
}
