<?php
declare(strict_types=1);

namespace Grifix\Acl\Application\Command\CreateRole;

use Grifix\Acl\Domain\Role\RoleFactoryInterface;
use Grifix\Acl\Domain\Role\RoleRepositoryInterface;

/**
 * Class CreateRoleCommandHandler
 * @package Grifix\Acl\Application\Command
 */
class CreateRoleCommandHandler
{

    /**
     * @var RoleFactoryInterface
     */
    protected $roleFactory;

    /**
     * @var RoleRepositoryInterface
     */
    protected $roleRepository;

    /**
     * CreateRoleCommandHandler constructor.
     * @param RoleFactoryInterface $roleFactory
     * @param RoleRepositoryInterface $roleRepository
     */
    public function __construct(
        RoleFactoryInterface $roleFactory,
        RoleRepositoryInterface $roleRepository
    ) {
        $this->roleFactory = $roleFactory;
        $this->roleRepository = $roleRepository;
    }

    /**
     * @param CreateRoleCommand $command
     */
    public function __invoke(CreateRoleCommand $command): void
    {
        $role = $this->roleFactory->createRole($command->getId(), $command->getName());
        $this->roleRepository->save($role);
    }
}
