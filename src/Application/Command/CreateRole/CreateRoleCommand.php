<?php
declare(strict_types=1);

namespace Grifix\Acl\Application\Command\CreateRole;

/**
 * Class CreateRoleCommand
 * @package Grifix\Acl\Application\Command
 */
class CreateRoleCommand
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * CreateRoleCommand constructor.
     * @param string $id
     * @param string $name
     */
    public function __construct(string $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
