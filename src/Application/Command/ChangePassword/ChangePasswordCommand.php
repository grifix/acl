<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Application\Command\ChangePassword;

/**
 * Class ChangePasswordCommand
 *
 * @category Grifix
 * @package  Grifix\AclService\Application\Command
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ChangePasswordCommand
{

    protected $userId;

    protected $oldPassword;

    protected $newPassword;

    protected $newPasswordAgain;

    /**
     * ChangePasswordCommand constructor.
     *
     * @param string $userId
     * @param string $oldPassword
     * @param string $newPassword
     * @param string $newPasswordAgain
     */
    public function __construct(string $userId, string $oldPassword, string $newPassword, string $newPasswordAgain)
    {
        $this->userId = $userId;
        $this->oldPassword = $oldPassword;
        $this->newPassword = $newPassword;
        $this->newPasswordAgain = $newPasswordAgain;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getOldPassword(): string
    {
        return $this->oldPassword;
    }

    /**
     * @return string
     */
    public function getNewPassword(): string
    {
        return $this->newPassword;
    }

    /**
     * @return string
     */
    public function getNewPasswordAgain(): string
    {
        return $this->newPasswordAgain;
    }
}
