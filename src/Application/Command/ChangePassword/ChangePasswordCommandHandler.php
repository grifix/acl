<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Application\Command\ChangePassword;

use Grifix\Acl\Domain\User\UserRepositoryInterface;

/**
 * Class ChangePasswordCommandHandler
 *
 * @category Grifix
 * @package  Grifix\AclService\Application\Command
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ChangePasswordCommandHandler
{

    /**
     * @var UserRepositoryInterface
     */
    protected $repository;

    /**
     * ChangePasswordCommandHandler constructor.
     * @param UserRepositoryInterface $repository
     */
    public function __construct(UserRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(ChangePasswordCommand $command): void
    {
        $user = $this->repository->find($command->getUserId());
        $user->changePassword($command->getOldPassword(), $command->getNewPassword(), $command->getNewPasswordAgain());
        $this->repository->save($user);
    }
}
