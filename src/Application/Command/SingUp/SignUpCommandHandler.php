<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Application\Command\SingUp;

use Grifix\Acl\Domain\User\UserFactoryInterface;
use Grifix\Acl\Domain\User\UserRepositoryInterface;

/**
 * Class SignUpCommandHandler
 *
 * @category Grifix
 * @package  Grifix\AclService\Domain\UserModel\Command
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class SignUpCommandHandler
{

    /**
     * @var UserFactoryInterface
     */
    protected $userFactory;

    /**
     * @var UserRepositoryInterface
     */
    protected $userRepository;

    /**
     * SignUpCommandHandler constructor.
     * @param UserFactoryInterface $userFactory
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserFactoryInterface $userFactory, UserRepositoryInterface $userRepository)
    {
        $this->userFactory = $userFactory;
        $this->userRepository = $userRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(SignUpCommand $command): void
    {
        $user = $this->userFactory->createUser($command->getId(), $command->getEmail());
        $this->userRepository->save($user);
    }
}
