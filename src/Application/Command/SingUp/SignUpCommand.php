<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Application\Command\SingUp;

use Grifix\Kit\Type\EmailInterface;

/**
 * Class SignUpCommand
 *
 * @category Grifix
 * @package  Grifix\AclService\Domain\UserModel\Command
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class SignUpCommand
{
    /**
     * @var int
     */
    protected $id;
    
    /**
     * @var EmailInterface
     */
    protected $email;
    
    /**
     * SignUpCommand constructor.
     *
     * @param string   $id
     * @param EmailInterface $email
     */
    public function __construct(string $id, EmailInterface $email)
    {
        $this->email = $email;
        $this->id = $id;
    }
    
    /**
     * @return EmailInterface
     */
    public function getEmail(): EmailInterface
    {
        return $this->email;
    }
    
    /**
     * @return int
     */
    public function getId(): string
    {
        return $this->id;
    }
}
