<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Application\Command\SingOut;

use Grifix\Acl\Application\Command\SingOut\Exception\UserIsNotSignedInException;
use Grifix\Acl\Domain\User\Exception\UserNotExistsException;
use Grifix\Acl\Domain\User\UserRepositoryInterface;

/**
 * Class SignOutCommandHandler
 *
 * @category Grifix
 * @package  Grifix\Acl\Application\Command
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class SignOutCommandHandler
{

    /**
     * @var UserRepositoryInterface
     */
    protected $userRepository;


    /**
     * SignOutCommandHandler constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(SignOutCommand $command): void
    {
        try {
            $user = $this->userRepository->findBySessionId($command->getSessionId());
        } catch (UserNotExistsException $e) {
            throw new UserIsNotSignedInException();
        }

        $user->signOut();
        $this->userRepository->save($user);
    }
}
