<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Acl\Application\Command\SingOut;

/**
 * Class SignOutCommand
 *
 * @category Grifix
 * @package  Grifix\Acl\Application\Command
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class SignOutCommand
{
    protected $sessionId;
    
    /**
     * SignOutCommand constructor.
     *
     * @param $sessionId
     */
    public function __construct(string $sessionId)
    {
        $this->sessionId = $sessionId;
    }
    
    /**
     * @return string
     */
    public function getSessionId(): string
    {
        return $this->sessionId;
    }
}
