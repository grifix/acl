<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Application\Command\ConfirmEmail;

use Grifix\Acl\Domain\User\UserRepositoryInterface;

/**
 * Class ConfirmEmailCommandHandler
 *
 * @category Grifix
 * @package  Grifix\AclService\Application\Command
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ConfirmEmailCommandHandler
{
    /**
     * @var UserRepositoryInterface
     */
    protected $repository;

    /**
     * ConfirmEmailCommandHandler constructor.
     * @param UserRepositoryInterface $repository
     */
    public function __construct(UserRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(ConfirmEmailCommand $command): void
    {
        $user = $this->repository->findByEmail($command->getEmail());

        $user->confirmEmail($command->getToken());

        $this->repository->save($user);
    }
}
