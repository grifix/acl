<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Acl\Application\Command\ConfirmEmail;

use Grifix\Kit\Type\Email;

/**
 * Class ConfirmEmailCommand
 *
 * @category Grifix
 * @package  Grifix\AclService\Application\Command
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ConfirmEmailCommand
{
    /**
     * @var Email
     */
    protected $email;
    
    /**
     * @var string
     */
    protected $token;
    
    /**
     * ConfirmEmailCommand constructor.
     *
     * @param Email  $email
     * @param string $token
     */
    public function __construct(Email $email, string $token)
    {
        $this->email = $email;
        $this->token = $token;
    }
    
    
    /**
     * @return Email
     */
    public function getEmail(): Email
    {
        return $this->email;
    }
    
    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }
}
