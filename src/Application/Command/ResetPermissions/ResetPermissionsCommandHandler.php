<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Application\Command\ResetPermissions;

use Grifix\Acl\Domain\Role\RoleInterface;
use Grifix\Acl\Domain\Role\RoleRepositoryInterface;
use Grifix\Acl\Ui\Permission;
use Grifix\Kit\Config\ConfigInterface;
use Grifix\Kit\Kernel\KernelInterface;

/**
 * Class ResetPermissionsCommandHandler
 *
 * @category Grifix
 * @package  Grifix\AclService\Application\Command
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ResetPermissionsCommandHandler
{


    /**
     * @var RoleRepositoryInterface
     */
    protected $roleRepository;

    /**
     * @var KernelInterface
     */
    protected $kernel;

    /**
     * @var ConfigInterface
     */
    protected $config;

    /**
     * ResetPermissionsCommandHandler constructor.
     * @param RoleRepositoryInterface $roleRepository
     * @param KernelInterface $kernel
     * @param ConfigInterface $config
     */
    public function __construct(
        RoleRepositoryInterface $roleRepository,
        KernelInterface $kernel,
        ConfigInterface $config
    ) {
        $this->roleRepository = $roleRepository;
        $this->kernel = $kernel;
        $this->config = $config;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(ResetPermissionsCommand $command): void
    {
        $role = $this->roleRepository->findByName($command->getRoleName());

        $role->clearPermissions();

        $this->grantPermissions($command->getRoleName(), $role);

        $this->roleRepository->save($role);
    }


    protected function grantPermissions(string $roleName, RoleInterface $role)
    {
        foreach ($this->kernel->getModules() as $module) {
            $prefix = $module->getVendor() . '.' . $module->getName();
            $permissions = $this->config->get($prefix . '.acl.permissions', []);
            foreach ($permissions as $permission) {
                $this->grantPermission($permission, $roleName, $role);
            }
        }
    }

    protected function grantPermission(Permission $permission, string $roleName, RoleInterface $role): void
    {

        if (!in_array($roleName, $permission->getRoles())) {
            return;
        }

        $role->grantPermission($permission->getName());
    }
}
