<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Application\Command\ResetPermissions;

/**
 * Class ResetPermissionsCommand
 *
 * @category Grifix
 * @package  Grifix\AclService\Application\Command
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ResetPermissionsCommand
{
    protected $roleName;
    
    /**
     * ResetPermissionsCommand constructor.
     *
     * @param string $roleName
     */
    public function __construct(string $roleName)
    {
        $this->roleName = $roleName;
    }
    
    /**
     * @return string
     */
    public function getRoleName()
    {
        return $this->roleName;
    }
}
