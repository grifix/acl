<?php
declare(strict_types=1);

namespace Grifix\Acl\Application\Listener;

use Grifix\Acl\Domain\User\Event\PasswordWasResetEvent;

/**
 * Class ResetPasswordListener
 * @package Grifix\Acl\Application\Listener
 */
class ResetPasswordListener
{
    /**
     * @param PasswordWasResetEvent $event
     */
    public function __invoke(PasswordWasResetEvent $event)
    {
        // TODO: Implement __invoke() method.
    }
}