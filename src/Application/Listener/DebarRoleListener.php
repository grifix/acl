<?php
declare(strict_types=1);

namespace Grifix\Acl\Application\Listener;

use Grifix\Acl\Application\Contract\RelationsManagerInterface;
use Grifix\Acl\Domain\User\Event\RoleDebarredEvent;

/**
 * Class AssignRoleListener
 * @package Grifix\Acl\Application\Listener
 */
class DebarRoleListener
{
    protected $roleManager;

    /**
     * AssignRoleListener constructor.
     * @param RelationsManagerInterface $roleManager
     */
    public function __construct(RelationsManagerInterface $roleManager)
    {
        $this->roleManager = $roleManager;
    }

    /**
     * @param RoleDebarredEvent $event
     */
    public function __invoke(RoleDebarredEvent $event)
    {
        $this->roleManager->removeRelation($event->getUserId(), $event->getRoleId());
    }
}
