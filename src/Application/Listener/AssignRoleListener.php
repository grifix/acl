<?php
declare(strict_types=1);

namespace Grifix\Acl\Application\Listener;

use Grifix\Acl\Application\Contract\RelationsManagerInterface;
use Grifix\Acl\Domain\User\Event\RoleAssignedEvent;

/**
 * Class AssignRoleListener
 * @package Grifix\Acl\Application\Listener
 */
class AssignRoleListener
{
    protected $roleManager;

    /**
     * AssignRoleListener constructor.
     * @param RelationsManagerInterface $roleManager
     */
    public function __construct(RelationsManagerInterface $roleManager)
    {
        $this->roleManager = $roleManager;
    }

    /**
     * @param RoleAssignedEvent $event
     */
    public function __invoke(RoleAssignedEvent $event)
    {
        $this->roleManager->removeRelation($event->getUserId(), $event->getRoleId());
        $this->roleManager->createRelation($event->getUserId(), $event->getRoleId());
    }
}
