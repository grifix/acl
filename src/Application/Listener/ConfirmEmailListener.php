<?php
declare(strict_types=1);

namespace Grifix\Acl\Application\Listener;

use Grifix\Acl\Domain\User\Event\EmailConfirmedEvent;

/**
 * Class ConfirmEmailListener
 * @package Grifix\Acl\Application\Listener
 */
class ConfirmEmailListener
{
    /**
     * @param EmailConfirmedEvent $event
     */
    public function __invoke(EmailConfirmedEvent $event)
    {
        // TODO: Implement __invoke() method.
    }
}