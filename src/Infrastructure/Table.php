<?php
declare(strict_types=1);

namespace Grifix\Acl\Infrastructure;

/**
 * Interface Table
 * @package Grifix\Acl\Infrastructure
 */
interface Table
{
    public const USER = 'grifix_acl."user"';
    public const ROLE = 'grifix_acl."role"';
    public const USER_TO_ROLE = 'grifix_acl."user2role"';
}
