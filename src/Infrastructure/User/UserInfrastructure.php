<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Infrastructure\User;

use Grifix\Acl\Domain\Role\RoleRepositoryInterface;
use Grifix\Acl\Domain\User\UserInfrastructureInterface;
use Grifix\Acl\Infrastructure\User\Password\PasswordGeneratorInterface;
use Grifix\Kit\Mailer\MailerInterface;
use Grifix\Kit\Orm\EventCollector\EventCollectorInterface;
use Grifix\Kit\Orm\EventCollector\EventPublisherTrait;

/**
 * Class UserInfrastructure
 *
 * @category Grifix
 * @package  Grifix\AclService\Infrastructure\User
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class UserInfrastructure implements UserInfrastructureInterface
{

    use EventPublisherTrait;


    /**
     * @var MailerInterface
     */
    protected $mailer;

    /**
     * @var int
     */
    protected $maxSignInAttempts;

    /**
     * @var \DateInterval
     */
    protected $bruteForceBlockInterval;

    /**
     * @var PasswordGeneratorInterface
     */
    protected $passwordGenerator;

    /**
     * @var RoleRepositoryInterface
     */
    protected $roleRepository;

    /**
     * UserInfrastructure constructor.
     *
     * @param EventCollectorInterface $eventCollector
     * @param MailerInterface $mailer
     * @param int $maxSingInAttempts <cfg:grifix.acl.signIn.maxAttempts>
     * @param string $bruteForceBlockInterval <cfg:grifix.acl.signIn.bruteForceBlockInterval>
     * @param PasswordGeneratorInterface $passwordGenerator
     * @param RoleRepositoryInterface $roleRepository
     *
     * @throws \Exception
     */
    public function __construct(
        EventCollectorInterface $eventCollector,
        MailerInterface $mailer,
        int $maxSingInAttempts,
        string $bruteForceBlockInterval,
        PasswordGeneratorInterface $passwordGenerator,
        RoleRepositoryInterface $roleRepository
    ) {
        $this->eventCollector = $eventCollector;
        $this->mailer = $mailer;
        $this->maxSignInAttempts = $maxSingInAttempts;
        $this->bruteForceBlockInterval = new \DateInterval($bruteForceBlockInterval);
        $this->passwordGenerator = $passwordGenerator;
        $this->roleRepository = $roleRepository;
    }

    /**
     * @return \DateInterval
     */
    public function getBruteForceBlockInterval(): \DateInterval
    {
        return $this->bruteForceBlockInterval;
    }

    /**
     * @param string $email
     * @param string $token
     *
     * @return void
     * @throws \Grifix\Kit\Mailer\Exception\InvalidMessageClassException
     */
    public function sendConfirmationMessage(string $email, string $token): void
    {
        $message = $this->mailer->createMessage()->setBody($token)->setTo($email)->setSubject('email confirmation');
        $this->mailer->send($message);
    }

    /**
     * {@inheritdoc}
     */
    public function getDefaultTimezone(): string
    {
        return date_default_timezone_get();
    }

    /**
     * {@inheritdoc}
     */
    public function getMaxSignInAttempts()
    {
        return $this->maxSignInAttempts;
    }

    /**
     * @return string
     */
    public function generatePassword(): string
    {
        return $this->passwordGenerator->generatePassword();
    }

    /**
     * {@inheritdoc}
     */
    public function roleExists(string $roleId): bool
    {
        return $this->roleRepository->exists($roleId);
    }
}
