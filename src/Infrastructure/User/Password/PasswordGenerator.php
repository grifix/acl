<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Infrastructure\User\Password;

use Grifix\Kit\Type\RangeInterface;
use Hackzilla\PasswordGenerator\Generator\RequirementPasswordGenerator;

/**
 * Class PasswordGenerator
 *
 * @category Grifix
 * @package  Grifix\AclService\Infrastructure\Password
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class PasswordGenerator implements PasswordGeneratorInterface
{
    /**
     * @var RequirementPasswordGenerator
     */
    protected $passwordGenerator;

    /**
     * PasswordGenerator constructor.
     *
     * @param RequirementPasswordGenerator $computerPasswordGenerator
     * @param int                          $length    <cfg:grifix.acl.password.length>
     * @param RangeInterface               $upperCase <cfg:grifix.acl.password.upperCase>
     * @param RangeInterface               $loverCase <cfg:grifix.acl.password.loverCase>
     * @param RangeInterface               $numbers   <cfg:grifix.acl.password.numbers>
     * @param RangeInterface               $symbols   <cfg:grifix.acl.password.symbols>
     */
    public function __construct(
        RequirementPasswordGenerator $computerPasswordGenerator,
        int $length,
        RangeInterface $upperCase = null,
        RangeInterface $loverCase = null,
        RangeInterface $numbers = null,
        RangeInterface $symbols = null
    ) {
        $this->passwordGenerator = $computerPasswordGenerator;
        $this->passwordGenerator->setLength($length);
        if ($upperCase) {
            $this->passwordGenerator->setUppercase(true);
            $this->passwordGenerator->setMinimumCount(
                RequirementPasswordGenerator::OPTION_UPPER_CASE,
                $upperCase->getFrom()
            );
            $this->passwordGenerator->setMaximumCount(
                RequirementPasswordGenerator::OPTION_UPPER_CASE,
                $upperCase->getTo()
            );
        } else {
            $this->passwordGenerator->setUppercase(false);
        }

        if ($loverCase) {
            $this->passwordGenerator->setLowercase(true);
            $this->passwordGenerator->setMinimumCount(
                RequirementPasswordGenerator::OPTION_LOWER_CASE,
                $loverCase->getFrom()
            );
            $this->passwordGenerator->setMaximumCount(
                RequirementPasswordGenerator::OPTION_LOWER_CASE,
                $loverCase->getTo()
            );
        } else {
            $this->passwordGenerator->setLowercase(false);
        }

        if ($numbers) {
            $this->passwordGenerator->setNumbers(true);
            $this->passwordGenerator->setMinimumCount(
                RequirementPasswordGenerator::OPTION_NUMBERS,
                $numbers->getFrom()
            );
            $this->passwordGenerator->setMaximumCount(
                RequirementPasswordGenerator::OPTION_NUMBERS,
                $numbers->getTo()
            );
        } else {
            $this->passwordGenerator->setNumbers(false);
        }

        if ($symbols) {
            $this->passwordGenerator->setSymbols(true);
            $this->passwordGenerator->setMinimumCount(
                RequirementPasswordGenerator::OPTION_SYMBOLS,
                $symbols->getFrom()
            );
            $this->passwordGenerator->setMaximumCount(
                RequirementPasswordGenerator::OPTION_SYMBOLS,
                $symbols->getTo()
            );
        } else {
            $this->passwordGenerator->setSymbols(false);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function generatePassword()
    {
        return $this->passwordGenerator->generatePassword();
    }

    /**
     * {@inheritdoc}
     */
    public function validatePassword(string $password): bool
    {
        return $this->passwordGenerator->validatePassword($password);
    }
}