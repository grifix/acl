<?php
declare(strict_types=1);

namespace Grifix\Acl\Infrastructure\User\Password;

use Grifix\Acl\Domain\User\Password\Password;
use Grifix\Acl\Domain\User\Password\PasswordInfrastructureInterface;
use Grifix\Kit\Conversion\Converter\AbstractConverter;

/**
 * Class PasswordHydrationConverter
 *
 * @package Grifix\Acl\Infrastructure\User\Password
 */
class PasswordRestoringConverter extends AbstractConverter
{
    protected $passwordInfrastructure;

    /**
     * PasswordHydrationConverter constructor.
     *
     * @param PasswordInfrastructureInterface $passwordInfrastructure
     */
    public function __construct(PasswordInfrastructureInterface $passwordInfrastructure)
    {
        $this->passwordInfrastructure = $passwordInfrastructure;
    }

    /**
     * @param mixed $value
     *
     * @return mixed|object
     * @throws \ReflectionException
     */
    protected function doConvert($value)
    {
        $reflectionClass = new \ReflectionClass(Password::class);
        $result = $reflectionClass->newInstanceWithoutConstructor();
        $valueProperty = $reflectionClass->getProperty('value');
        $valueProperty->setAccessible(true);
        $valueProperty->setValue($result, $value);
        $valueProperty->setAccessible(false);
        $infrastructureProperty = $reflectionClass->getProperty('infrastructure');
        $infrastructureProperty->setAccessible(true);
        $infrastructureProperty->setValue($result, $this->passwordInfrastructure);
        $infrastructureProperty->setAccessible(false);
        return $result;
    }
}
