<?php

declare(strict_types=1);

namespace Grifix\Acl\Infrastructure\User\Password;

use Grifix\Acl\Domain\User\Password\PasswordInfrastructureInterface;
use Grifix\Kit\Mailer\MailerInterface;

/**
 * Class PasswordInfrastructure
 *
 * @package Grifix\Acl\Infrastructure\User\Password
 */
class PasswordInfrastructure implements PasswordInfrastructureInterface
{

    /**
     * @var PasswordGeneratorInterface
     */
    protected $generator;

    /**
     * @var PasswordHasherInterface
     */
    protected $hasher;

    /**
     * @var MailerInterface
     */
    protected $mailer;

    /**
     * PasswordInfrastructure constructor.
     *
     * @param PasswordGeneratorInterface $generator
     * @param PasswordHasherInterface    $hasher
     * @param MailerInterface            $mailer
     */
    public function __construct(
        PasswordGeneratorInterface $generator,
        PasswordHasherInterface $hasher,
        MailerInterface $mailer
    ) {
        $this->generator = $generator;
        $this->hasher = $hasher;
        $this->mailer = $mailer;
    }

    /**
     * @return string
     */
    public function generatePassword(): string
    {
        return $this->generator->generatePassword();
    }

    /**
     * {@inheritdoc}
     */
    public function validatePassword(string $password): bool
    {
        return $this->generator->validatePassword($password);
    }

    /**
     * {@inheritdoc}
     */
    public function hashPassword(string $password): string
    {
        return $this->hasher->hashPassword($password);
    }

    /**
     * @param string $password
     * @param string $hash
     *
     * @return bool
     */
    public function verifyPassword(string $password, string $hash): bool
    {
        return $this->hasher->verifyPassword($password, $hash);
    }
}
