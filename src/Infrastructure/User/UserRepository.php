<?php
declare(strict_types=1);

namespace Grifix\Acl\Infrastructure\User;

use Grifix\Acl\Domain\User\UserInterface;
use Grifix\Acl\Domain\User\UserRepositoryInterface;
use Grifix\Acl\Domain\User\Exception\UserNotExistsException;
use Grifix\Kit\Collection\CollectionWrapperTrait;
use Grifix\Kit\Orm\Repository\RepositoryInterface;
use Grifix\Kit\Type\EmailInterface;

/**
 * Class UserRepository
 * @package Grifix\Acl\Infrastructure\User
 */
class UserRepository implements UserRepositoryInterface
{
    /**
     * @var RepositoryInterface
     */
    protected $repository;

    use CollectionWrapperTrait;

    /**
     * UserRepository constructor.
     * @param RepositoryInterface $repository
     */
    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * {@inheritdoc}
     */
    public function find(string $id): UserInterface
    {
        $result = $this->repository->find($id);
        if (!$result) {
            throw new UserNotExistsException('id', $id);
        }
        /**@var $result UserInterface */
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function save(UserInterface $user): void
    {
        $this->repository->save($user);
    }

    /**
     * {@inheritdoc}
     */
    public function delete(UserInterface $user): void
    {
        $this->repository->delete($user);
    }

    /**
     * {@inheritdoc}
     */
    public function findByEmail(EmailInterface $email): UserInterface
    {
        return $this->getBy('email.email', strval($email));
    }

    /**
     * {@inheritdoc}
     */
    public function findBySessionId(string $sessionId): UserInterface
    {
        return $this->getBy('sessionId', $sessionId);
    }

    /**
     * @param string $propertyName
     * @param $propertyValue
     * @return UserInterface
     * @throws UserNotExistsException
     * @throws \ReflectionException
     */
    protected function getBy(string $propertyName, $propertyValue): UserInterface
    {
        $result = $this->repository->findBy($propertyName, $propertyValue)->first();
        if (!$result) {
            throw new UserNotExistsException($propertyName, $propertyValue);
        }

        return $result;
    }
}
