<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Infrastructure\User;

use Grifix\Acl\Domain\User\User;
use Grifix\Acl\Infrastructure\Table;
use Grifix\Common\Infrastructure\Serializer\ObjectSerializer;
use Grifix\Kit\Orm\Blueprint\AbstractBlueprint;

/**
 * Class UserBlueprint
 *
 * @category Grifix
 * @package  Grifix\AclService\Infrastructure\User
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class UserBlueprint extends AbstractBlueprint
{
    protected $entityClass = User::class;

    protected $table = Table::USER;
    
    protected $serializerClass = ObjectSerializer::class;

    /**
     * {@inheritdoc}
     */
    protected function init(): void
    {
    }
}
