<?php
declare(strict_types=1);

namespace Grifix\Acl\Infrastructure\User\Email;

use Grifix\Acl\Domain\User\Email\UserEmailInfrastructureInterface;
use Grifix\Acl\Infrastructure\Table;
use Grifix\Kit\Db\ConnectionInterface;

/**
 * Class UserUserEmailInfrastructure
 * @package Grifix\Acl\Infrastructure\User\Email
 */
class UserUserEmailInfrastructure implements UserEmailInfrastructureInterface
{
    /**
     * @var ConnectionInterface
     */
    protected $connection;

    /**
     * UserUserEmailInfrastructure constructor.
     * @param ConnectionInterface $connection
     */
    public function __construct(ConnectionInterface $connection)
    {
        $this->connection = $connection;
    }

    /**
     * {@inheritdoc}
     */
    public function emailExists($email): bool
    {
        $dbQuery = $this->connection->createQuery()
            ->select('id')
            ->from(Table::USER)
            ->where("data->'email'->>'email' = :email")->bindValue('email', $email);

        $record = $dbQuery->fetch();
        if ($record) {
            return true;
        }
        return false;
    }
}