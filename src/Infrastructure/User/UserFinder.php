<?php
declare(strict_types=1);

namespace Grifix\Acl\Infrastructure\User;

use Grifix\Acl\Application\Contract\UserFinder\UserFilter;
use Grifix\Acl\Application\Contract\UserFinder\UserDto;
use Grifix\Acl\Application\Contract\UserFinder\UserFinderInterface;
use Grifix\Acl\Domain\User\UserInterface;
use Grifix\Acl\Infrastructure\Table;
use Grifix\Kit\Collection\CollectionFactoryInterface;
use Grifix\Kit\Collection\CollectionInterface;
use Grifix\Kit\Conversion\ConversionFactoryInterface;
use Grifix\Kit\Conversion\Converter\StringToBoolConverter;
use Grifix\Kit\Conversion\Converter\StringToDateConverter;
use Grifix\Kit\Conversion\Converter\StringToIntConverter;
use Grifix\Kit\Conversion\Converter\StringToIpConverter;
use Grifix\Kit\Conversion\Converter\StringToTimeZoneConverter;
use Grifix\Kit\Db\ConnectionInterface;
use Grifix\Kit\Db\Query\QueryInterface;

/***
 * Class UserFinder
 * @package Grifix\Acl\Infrastructure\User
 */
class UserFinder implements UserFinderInterface
{
    /**
     * @var CollectionFactoryInterface
     */
    protected $collectionFactory;

    /**
     * @var ConnectionInterface
     */
    protected $connection;

    /**
     * @var ConversionFactoryInterface
     */
    protected $conversionFactory;

    /**
     * UserFinder constructor.
     * @param CollectionFactoryInterface $collectionFactory
     * @param ConnectionInterface $connection
     * @param ConversionFactoryInterface $conversionFactory
     */
    public function __construct(
        CollectionFactoryInterface $collectionFactory,
        ConnectionInterface $connection,
        ConversionFactoryInterface $conversionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->connection = $connection;
        $this->conversionFactory = $conversionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function find(UserFilter $filter, ?int $offset = null, ?int $limit = null): CollectionInterface
    {
        $query = $this->connection->createQuery()
            ->select('*')
            ->from(Table::USER);

        $this->applyFilter($filter, $query);

        if ($offset !== null) {
            $query->offset($offset);
        }

        if ($limit !== null) {
            $query->limit($limit);
        }

        $records = $query->fetchAll();

        $result = [];
        foreach ($records as $record) {
            $permissions = null;
            if ($filter->isWithPermissions()) {
                $permissions = $this->fetchUserPermissions($record['id']);
            }
            $result[] = $this->createUserDto(json_decode($record['data'], true), $permissions);
        }

        return $this->collectionFactory->createCollection($result);
    }

    public function count(UserFilter $filter): int
    {
        $query = $this->connection->createQuery()
            ->select('COUNT(*) AS result')
            ->from(Table::USER);
        $this->applyFilter($filter, $query);
        return $query->fetch()['result'];
    }

    protected function applyFilter(UserFilter $filter, QueryInterface $query): void
    {
        if ($filter->getId()) {
            $query->where('id = :id')->bindValue('id', $filter->getId());
        }

        if ($filter->isGuest()) {
            $query->where('id = :id')->bindValue('id', UserInterface::GUEST_ID);
        }

        if ($filter->getSessionId()) {
            $query->where("data->>'sessionId' = :sessionId")->bindValue('sessionId', $filter->getSessionId());
        }
    }

    /**
     * @param array $record
     * @param array|null $permissions
     * @return UserDto
     */
    protected function createUserDto(array $record, ?array $permissions = null): UserDto
    {
        $conversion = $this->conversionFactory->createConversion();
        $conversion->createField('registrationDate')->createConverter(StringToDateConverter::class);
        $conversion->createField('isActive')->createConverter(StringToBoolConverter::class);
        $conversion->createField('emailConfirmed')->createConverter(StringToBoolConverter::class);
        $conversion->createField('bruteForceCounter')->createConverter(StringToIntConverter::class);
        $conversion->createField('activationDate')->createConverter(StringToDateConverter::class);
        $conversion->createField('lastSignInDate')->createConverter(StringToDateConverter::class);
        $conversion->createField('lastIp')->createConverter(StringToIpConverter::class);
        $conversion->createField('timeZone')->createConverter(StringToTimeZoneConverter::class);
        $conversion->convert($record);
        return new UserDto(
            strval($record['id']),
            $record['email']['email'],
            $record['registrationDate'],
            $record['isActive'],
            $record['email']['confirmed'],
            $record['bruteForceCounter'],
            ($record['id'] == UserInterface::GUEST_ID),
            $record['lastSignInDate'],
            $record['lastIp'],
            $record['timeZone'],
            $record['token'],
            $permissions
        );
    }

    /**
     * @param string $userId
     * @return array
     */
    protected function fetchUserPermissions(string $userId)
    {
        $rows = $this->connection->select(
            "SELECT jsonb_array_elements(r.data->'permissions') as permission
                    FROM " . Table::ROLE . " AS r
                    INNER JOIN " . Table::USER_TO_ROLE . " AS ur ON ur.role_id = r.id
                    WHERE ur.user_id = :user_id",
            ['user_id' => $userId]
        );

        $result = [];
        foreach ($rows as $row) {
            $result[] = json_decode($row['permission']);
        }
        return $result;
    }
}
