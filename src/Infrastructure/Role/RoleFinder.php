<?php
declare(strict_types=1);

namespace Grifix\Acl\Infrastructure\Role;


use Grifix\Acl\Application\Contract\RoleFinder\RoleFilter;
use Grifix\Acl\Application\Contract\RoleFinder\RoleDto;
use Grifix\Acl\Application\Contract\RoleFinder\RoleFinderInterface;
use Grifix\Acl\Infrastructure\Table;
use Grifix\Kit\Collection\CollectionFactoryInterface;
use Grifix\Kit\Collection\CollectionInterface;
use Grifix\Kit\Db\ConnectionInterface;

/**
 * Class RoleFinder
 * @package Grifix\Acl\Infrastructure\Role
 */
class RoleFinder implements RoleFinderInterface
{

    /**
     * @var ConnectionInterface
     */
    private $connection;

    /**
     * @var CollectionFactoryInterface
     */
    private $collectionFactory;

    /**
     * RoleFinder constructor.
     * @param ConnectionInterface $connection
     * @param CollectionFactoryInterface $collectionFactory
     */
    public function __construct(ConnectionInterface $connection, CollectionFactoryInterface $collectionFactory)
    {
        $this->connection = $connection;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function find(RoleFilter $filter): CollectionInterface
    {
        $dbQuery = $this->connection->createQuery()
            ->select('*')
            ->from(Table::ROLE);

        $records = $dbQuery->fetchAll();
        $result = $this->collectionFactory->createCollection();
        foreach ($records as $record) {
            $data = json_decode($record['data'], true);
            $result->add(new RoleDto($record['id'], $data['name']));
        }

        return $result;
    }
}
