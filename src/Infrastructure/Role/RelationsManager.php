<?php
declare(strict_types=1);

namespace Grifix\Acl\Infrastructure\Role;

use Grifix\Acl\Application\Contract\RelationsManagerInterface;
use Grifix\Acl\Infrastructure\Table;
use Grifix\Kit\Db\ConnectionInterface;

/**
 * Class RoleSynchronizer
 * @package Grifix\Acl\Infrastructure\Role
 */
class RelationsManager implements RelationsManagerInterface
{
    /**
     * @var ConnectionInterface
     */
    protected $connection;

    /**
     * RoleManager constructor.
     * @param ConnectionInterface $connection
     */
    public function __construct(ConnectionInterface $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param string $userId
     * @param string $roleId
     * @throws \Grifix\Kit\Db\Exception\StatementExecuteException
     */
    public function removeRelation(string $userId, string $roleId)
    {
        $this->connection->delete([
            'user_id' => $userId,
            'role_id' => $roleId
        ], Table::USER_TO_ROLE);
    }

    /**
     * @param string $userId
     * @param string $roleId
     * @throws \Grifix\Kit\Db\Exception\StatementExecuteException
     */
    public function createRelation(string $userId, string $roleId)
    {
        $this->connection->insert([
            'user_id' => $userId,
            'role_id' => $roleId
        ], Table::USER_TO_ROLE, false);
    }

}
