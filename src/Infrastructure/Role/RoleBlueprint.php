<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Infrastructure\Role;

use Grifix\Acl\Domain\Role\Role;
use Grifix\Acl\Infrastructure\Table;
use Grifix\Common\Infrastructure\Serializer\ObjectSerializer;
use Grifix\Kit\Orm\Blueprint\AbstractBlueprint;

/**
 * Class RoleBlueprint
 *
 * @category Grifix
 * @package  Grifix\AclService\Infrastructure\Role
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class RoleBlueprint extends AbstractBlueprint
{
    protected $entityClass = Role::class;

    protected $table = Table::ROLE;

    protected $serializerClass = ObjectSerializer::class;

    /**
     * @return void
     * @throws \Throwable
     */
    protected function init(): void
    {
    }
}
