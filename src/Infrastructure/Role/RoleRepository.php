<?php
declare(strict_types=1);

namespace Grifix\Acl\Infrastructure\Role;

use Grifix\Acl\Domain\Role\Exception\RoleNotExistsException;
use Grifix\Acl\Domain\Role\Role;
use Grifix\Acl\Domain\Role\RoleInterface;
use Grifix\Acl\Domain\Role\RoleRepositoryInterface;
use Grifix\Kit\Collection\CollectionWrapperTrait;
use Grifix\Kit\Orm\Collection\CollectionInterface;
use Grifix\Kit\Orm\Repository\RepositoryInterface;

/**
 * Class RoleRepository
 * @package Grifix\Acl\Infrastructure\Role
 */
class RoleRepository implements RoleRepositoryInterface
{
    /**
     * @var RepositoryInterface
     */
    protected $repository;

    use CollectionWrapperTrait;

    /**
     * UserRepository constructor.
     * @param RepositoryInterface $repository
     */
    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * {@inheritdoc}
     */
    public function find(string $id): RoleInterface
    {
        $result = $this->repository->find($id);
        if (!$result) {
            throw new RoleNotExistsException('id', $id);
        }
        /**@var $result RoleInterface */
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function save(RoleInterface $role): void
    {
        $this->repository->save($role);
    }

    /**
     * {@inheritdoc}
     */
    public function delete(RoleInterface $role): void
    {
        $this->repository->delete($role);
    }

    /**
     * {@inheritdoc}
     */
    public function findByName(string $name): RoleInterface
    {
        return $this->getBy('name', $name);
    }

    /**
     * {@inheritdoc}
     */
    public function exists(string $id): bool
    {
        return boolval($this->repository->findBy('id', $id));
    }

    /**
     * @param string $propertyName
     * @param $propertyValue
     * @return RoleInterface
     * @throws RoleNotExistsException
     * @throws \ReflectionException
     */
    protected function getBy(string $propertyName, $propertyValue): RoleInterface
    {
        $result = $this->repository->findBy($propertyName, $propertyValue)->first();
        if (!$result) {
            throw new RoleNotExistsException($propertyName, $propertyValue);
        }

        return $result;
    }
}
