<?php

declare(strict_types = 1);

namespace Grifix\Acl\Infrastructure\Migration;

use Grifix\Acl\Infrastructure\Table;
use Grifix\Kit\Migration\AbstractMigration;

/**
 * Class M201712080659
 */

class M20180801043722 extends AbstractMigration
{
    public function up(): void
    {
        $this->execute('
            CREATE TABLE '.Table::USER_TO_ROLE.' (
              user_id VARCHAR NOT NULL,
              role_id VARCHAR NOT NULL
            ) 
            WITH (oids = false);
            
            ALTER TABLE '.Table::USER_TO_ROLE.'
              ADD CONSTRAINT user2role_idx 
                PRIMARY KEY (user_id, role_id);
                
            ALTER TABLE grifix_acl.user2role
              ADD CONSTRAINT user2role_fk FOREIGN KEY (user_id)
                REFERENCES '.Table::USER.'(id)
                ON DELETE CASCADE
                ON UPDATE CASCADE
                NOT DEFERRABLE;
                
            ALTER TABLE grifix_acl.user2role
              ADD CONSTRAINT user2role_fk2 FOREIGN KEY (role_id)
                REFERENCES '.Table::ROLE.'(id)
                ON DELETE CASCADE
                ON UPDATE CASCADE
                NOT DEFERRABLE;
        ');
    }
    
    public function down(): void
    {
        $this->execute('
            DROP TABLE IF EXISTS '.Table::USER_TO_ROLE.'
        ');
    }
}
