<?php

declare(strict_types=1);

namespace Grifix\Acl\Infrastructure\Migration;

use Grifix\Acl\Infrastructure\Table;
use Grifix\Kit\Migration\AbstractMigration;

/**
 * Class M201712080659
 */
class M20171214080828 extends AbstractMigration
{
    public function up(): void
    {
        $this->execute('
          CREATE TABLE ' . Table::ROLE . ' (
            id VARCHAR NOT NULL,
            data JSONB NOT NULL,
            PRIMARY KEY(id)
          ) 
          WITH (OIDS = FALSE);
          CREATE INDEX idx_role_data ON '.Table::ROLE.' USING GIN (data);
        ');
    }

    public function down(): void
    {
        $this->execute('
            DROP TABLE IF EXISTS ' . Table::ROLE . '
        ');
    }
}
