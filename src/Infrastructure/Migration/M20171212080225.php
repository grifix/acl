<?php

declare(strict_types = 1);

namespace Grifix\Acl\Infrastructure\Migration;

use Grifix\Kit\Migration\AbstractMigration;

/**
 * Class M201712080659
 */

class M20171212080225 extends AbstractMigration
{
    public function up(): void
    {
        $this->execute('
             CREATE SCHEMA IF NOT EXISTS "grifix_acl";
        ');
    }
    
    public function down(): void
    {
        $this->execute('
             DROP SCHEMA IF EXISTS "grifix_acl";
        ');
    }
}
