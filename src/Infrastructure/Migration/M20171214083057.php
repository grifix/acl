<?php

declare(strict_types=1);

namespace Grifix\Acl\Infrastructure\Migration;

use Grifix\Acl\Infrastructure\Table;
use Grifix\Kit\Migration\AbstractMigration;

/**
 * Class M201712080659
 */
class M20171214083057 extends AbstractMigration
{
    public function up(): void
    {
        $this->execute('
            CREATE TABLE '.Table::USER.' (
              "id" VARCHAR NOT NULL,
              "data" JSONB NOT NULL,
              PRIMARY KEY(id)
            ) 
            WITH (oids = false);
            CREATE INDEX idx_user_data ON '.Table::USER.' USING GIN (data);
        ');
    }

    public function down(): void
    {
        $this->execute('
            DROP TABLE IF EXISTS '.Table::USER.'
        ');
    }
}
