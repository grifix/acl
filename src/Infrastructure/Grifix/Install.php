<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Infrastructure\Grifix;

use Grifix\Acl\Application\Command\CreateRole\CreateRoleCommand;
use Grifix\Acl\Application\Command\CreateUser\CreateUserCommand;
use Grifix\Acl\Application\Command\ResetPermissions\ResetPermissionsCommand;
use Grifix\Acl\Application\Query\FindRoles\FindRolesQuery;
use Grifix\Acl\Application\Contract\RoleFinder\RoleDto;
use Grifix\Acl\Application\Query\GeneratePassword\GeneratePasswordQuery;
use Grifix\Acl\Domain\Role\RoleInterface;
use Grifix\Acl\Domain\User\UserInterface;
use Grifix\Kit\Cqrs\Command\CommandBusInterface;
use Grifix\Kit\Cqrs\Query\QueryBusInterface;
use Grifix\Kit\Kernel\Module\ModuleCommandInterface;
use Grifix\Kit\Kernel\Module\ModuleClassTrait;
use Grifix\Kit\Type\Email;
use Grifix\Kit\Type\EmailInterface;

/**
 * Class Install
 *
 * @category Grifix
 * @package  Grifix\AclService\Infrastructure\Grifix
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Install implements ModuleCommandInterface
{
    use ModuleClassTrait;

    /**
     * @var QueryBusInterface
     */
    protected $queryBus;

    /**
     * @var CommandBusInterface;
     */
    protected $commandBus;

    protected function init()
    {
        $this->queryBus = $this->getShared(QueryBusInterface::class);
        $this->commandBus = $this->getShared(CommandBusInterface::class);
    }

    public function run(): void
    {
        $this->init();
        $this->createRoles();
        $this->createGuest();

        $adminEmail = 'admin@grifix.net';
        $adminPassword = $this->queryBus->execute(new GeneratePasswordQuery());
        $this->createAdmin(new Email($adminEmail), $adminPassword);

        $this->resetPermissions();

        echo "\nACL module was installed\nadmin login: " . $adminEmail . "\npassword: " . $adminPassword . "\n\n";
    }

    /**
     * @throws \Throwable
     */
    protected function resetPermissions()
    {
        /**@var $roles RoleDto[] */
        $roles = $this->queryBus->execute(new FindRolesQuery());
        foreach ($roles as $role) {
            $this->commandBus->execute(new ResetPermissionsCommand($role->getName()));
        }
    }

    protected function createRoles()
    {
        $this->commandBus->execute(new CreateRoleCommand(RoleInterface::ADMIN_ID, 'admin'));
        $this->commandBus->execute(new CreateRoleCommand(RoleInterface::GUEST_ID, 'guest'));
        $this->commandBus->execute(new CreateRoleCommand(RoleInterface::USER_ID, 'user'));
    }

    /**
     * @throws \Throwable
     */
    protected function createGuest()
    {
        $this->commandBus->execute(new CreateUserCommand(
            UserInterface::GUEST_ID,
            new Email('guest@grifix.net'),
            RoleInterface::GUEST_ID
        ));
    }


    /**
     * @param EmailInterface $email
     * @param string $password
     * @throws \Throwable
     */
    protected function createAdmin(EmailInterface $email, string $password): void
    {
        $this->commandBus->execute(new CreateUserCommand(
            UserInterface::ADMIN_ID,
            $email,
            RoleInterface::ADMIN_ID,
            $password
        ));
    }
}
