<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Infrastructure\Grifix;

use Grifix\Acl\Application\Listener\AssignRoleListener;
use Grifix\Acl\Application\Listener\DebarRoleListener;
use Grifix\Acl\Domain\Role\Role;
use Grifix\Acl\Domain\Role\RoleRepositoryInterface;
use Grifix\Acl\Domain\User\Event\RoleAssignedEvent;
use Grifix\Acl\Domain\User\Event\RoleDebarredEvent;
use Grifix\Acl\Domain\User\User;
use Grifix\Acl\Domain\User\UserRepositoryInterface;
use Grifix\Acl\Infrastructure\Role\RoleBlueprint;
use Grifix\Acl\Infrastructure\Role\RoleRepository;
use Grifix\Acl\Infrastructure\User\UserBlueprint;
use Grifix\Acl\Infrastructure\User\UserRepository;
use Grifix\Kit\EventBus\EventBusInterface;
use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Kernel\Module\ModuleCommandInterface;
use Grifix\Kit\Kernel\Module\ModuleClassTrait;
use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;
use Grifix\Kit\Orm\Repository\RepositoryFactoryInterface;

/**
 * Class AbstractFeatureContext
 *
 * @category Grifix
 * @package  Grifix\Common\Grifix
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Bootstrap implements ModuleCommandInterface
{
    use ModuleClassTrait;

    /**
     * @return void
     */
    public function run(): void
    {
        $eventBus = $this->getShared(EventBusInterface::class);
        $eventBus->listen(RoleDebarredEvent::class, $this->getIoc()->createNewInstance(DebarRoleListener::class));
        $eventBus->listen(RoleAssignedEvent::class, $this->getIoc()->createNewInstance(AssignRoleListener::class));

        $entityManager = $this->getShared(EntityManagerInterface::class);

        $entityManager->registerBlueprint(UserBlueprint::class);
        $entityManager->registerBlueprint(RoleBlueprint::class);

        $this->setShared(UserRepositoryInterface::class, function () {
            /**@var IocContainerInterface $this */
            return $this->get(RepositoryFactoryInterface::class)->createRepositoryWrapper(
                User::class,
                UserRepository::class
            );
        });

        $this->setShared(RoleRepositoryInterface::class, function () {
            /**@var IocContainerInterface $this */
            return $this->get(RepositoryFactoryInterface::class)->createRepositoryWrapper(
                Role::class,
                RoleRepository::class
            );
        });
    }
}
