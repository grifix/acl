<?php
declare(strict_types=1);

namespace Grifix\Acl\Domain\User\Event;

/**
 * Class PasswordWasResetEvent
 * @package Grifix\Acl\Domain\User\Event
 */
class PasswordWasResetEvent
{
    /**
     * @var string
     */
    protected $newPassword;

    /**
     * @var string
     */
    protected $userId;

    /**
     * @var string
     */
    protected $userEmail;

    /**
     * PasswordWasResetEvent constructor.
     * @param string $newPassword
     * @param string $userId
     * @param string $userEmail
     */
    public function __construct(string $newPassword, string $userId, string $userEmail)
    {
        $this->newPassword = $newPassword;
        $this->userId = $userId;
        $this->userEmail = $userEmail;
    }

    /**
     * @return string
     */
    public function getNewPassword(): string
    {
        return $this->newPassword;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getUserEmail(): string
    {
        return $this->userEmail;
    }
}
