<?php

declare(strict_types=1);

namespace Grifix\Acl\Domain\User\Event;

use Grifix\Acl\Domain\User\UserInterface;

/**
 * Class RoleAssignedEvent
 * @package Grifix\Acl\Domain\User\Event
 */
class RoleAssignedEvent
{
    protected $userId;

    protected $roleId;

    /**
     * RoleAssignedEvent constructor.
     * @param UserInterface $initiator
     * @param int $userId
     * @param int $roleId
     */
    public function __construct(string $userId, string $roleId)
    {
        $this->roleId = $roleId;
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getRoleId(): string
    {
        return $this->roleId;
    }
}
