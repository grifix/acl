<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Domain\User\Event;

/**
 * Class BruteForceDetectedEvent
 *
 * @category Grifix
 * @package  Grifix\AclService\Domain\User\Event
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class BruteForceDetectedEvent
{
    protected $userId;

    /**
     * BruteForceDetectedEvent constructor.
     *
     * @param $userId
     */
    public function __construct(string $userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getUserId(): string
    {
        return $this->userId;
    }
}
