<?php
declare(strict_types=1);

namespace Grifix\Acl\Domain\User\Event;

/**
 * Class EmailConfirmedEvent
 * @package Grifix\Acl\Domain\User\Event
 */
class EmailConfirmedEvent
{
    /**
     * @var string
     */
    protected $userId;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected $token;

    /**
     * @var string
     */
    protected $password;

    /**
     * EmailConfirmedEvent constructor.
     * @param string $userId
     * @param string $email
     * @param string $token
     * @param string $password
     */
    public function __construct(string $userId, string $email, string $token, string $password)
    {
        $this->userId = $userId;
        $this->email = $email;
        $this->token = $token;
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }
}
