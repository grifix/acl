<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Domain\User\Event;

use Grifix\Acl\Domain\User\UserInterface;

/**
 * Class UserCreatedEvent
 *
 * @category Grifix
 * @package  Grifix\AclService\Domain\User\Event
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class UserCreatedEvent
{
    /**
     * @var UserInterface
     */
    protected $userId;

    protected $token;

    /**
     * UserCreatedEvent constructor.
     * @param string $userId
     * @param string $token
     */
    public function __construct(string $userId, string $token)
    {
        $this->userId = $userId;
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

}