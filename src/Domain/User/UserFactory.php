<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Domain\User;

use Grifix\Acl\Domain\User\Email\UserEmailFactoryInterface;
use Grifix\Acl\Domain\User\Password\PasswordFactoryInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Type\EmailInterface;

/**
 * Class UserFactory
 *
 * @category Grifix
 * @package  Grifix\AclService\Infrastructure\User
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class UserFactory extends AbstractFactory implements UserFactoryInterface
{

    /**
     * @var UserInfrastructureInterface
     */
    protected $userInfrastructure;

    /**
     * @var PasswordFactoryInterface
     */
    protected $passwordFactory;

    /**
     * @var UserEmailFactoryInterface
     */
    protected $userEmailFactory;


    /**
     * UserFactory constructor.
     * @param ClassMakerInterface $classMaker
     * @param UserInfrastructureInterface $userInfrastructure
     * @param PasswordFactoryInterface $passwordFactory
     * @param UserEmailFactoryInterface $userEmailFactory
     */
    public function __construct(
        ClassMakerInterface $classMaker,
        UserInfrastructureInterface $userInfrastructure,
        PasswordFactoryInterface $passwordFactory,
        UserEmailFactoryInterface $userEmailFactory
    ) {
        parent::__construct($classMaker);
        $this->userInfrastructure = $userInfrastructure;
        $this->passwordFactory = $passwordFactory;
        $this->userEmailFactory = $userEmailFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function createUser(
        string $id,
        EmailInterface $email,
        string $password = null,
        bool $emailConfirmed = false
    ): UserInterface {
        $class = $this->makeClassName(User::class);
        return new $class(
            $this->userInfrastructure,
            $id,
            $this->userEmailFactory->createEmail($email, $emailConfirmed),
            $this->passwordFactory->createPassword($password),
            null
        );
    }
}