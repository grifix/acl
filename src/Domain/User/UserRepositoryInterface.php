<?php
declare(strict_types=1);

namespace Grifix\Acl\Domain\User;

use Grifix\Acl\Domain\User\Exception\UserNotExistsException;
use Grifix\Kit\Collection\GenericCollectionInterface;
use Grifix\Kit\Type\EmailInterface;

/**
 * Interface UserRepositoryInterface
 * @package Grifix\Acl\Domain\User
 */
interface UserRepositoryInterface extends GenericCollectionInterface
{

    /**
     * @param string $id
     * @throws UserNotExistsException
     * @return UserInterface
     */
    public function find(string $id): UserInterface;

    /**
     * @param UserInterface $user
     */
    public function save(UserInterface $user): void;

    /**
     * @param UserInterface $user
     */
    public function delete(UserInterface $user): void;

    /**
     * @param EmailInterface $email
     * @throws UserNotExistsException
     * @return UserInterface
     */
    public function findByEmail(EmailInterface $email): UserInterface;

    /**
     * @param string $sessionId
     * @throws UserNotExistsException
     * @return UserInterface
     */
    public function findBySessionId(string $sessionId): UserInterface;
}
