<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Domain\User\Exception;

/**
 * Class WrongTokenException
 *
 * @category Grifix
 * @package  Grifix\AclService\Domain\User
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class WrongTokenException extends \DomainException
{
    
    /**
     * @var int
     */
    protected $userId;
    
    /**
     * @var string
     */
    protected $token;
    
    /**
     * WrongTokenException constructor.
     *
     * @param string    $email
     * @param string $token
     */
    public function __construct(string $email, string $token)
    {
        $this->userId = $email;
        $this->token = $token;
        $this->message = 'Wrong token!';
        
        parent::__construct();
    }
}