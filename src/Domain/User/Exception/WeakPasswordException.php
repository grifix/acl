<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Acl\Domain\User\Exception;

/**
 * Class WeakPasswordException
 *
 * @category Grifix
 * @package  Grifix\AclService\Domain\User
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class WeakPasswordException extends \DomainException
{
    protected $password;
    
    /**
     * WeakPasswordException constructor.
     *
     * @param string $password
     */
    public function __construct(string $password)
    {
        $this->password = $password;
        $this->message = 'Weak password!';
        
        parent::__construct();
    }
}