<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Domain\User\Exception;

/**
 * Class UserIsNotActiveException
 *
 * @category Grifix
 * @package  Grifix\AclService\Domain\UserModel\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class UserIsNotActiveException extends \DomainException
{
    protected $userId;

    protected $activationDate;

    protected $blockingReason;

    /**
     * UserIsNotActiveException constructor.
     *
     * @param int $email
     * @param \DateTimeInterface|null $activationDate
     * @param string|null $blockingReason
     */
    public function __construct(
        string $email,
        \DateTimeInterface $activationDate = null,
        string $blockingReason = null
    ) {
        $this->userId = $email;
        $this->activationDate = $activationDate;
        $this->message = 'UserModel is not active!';
        $this->blockingReason = $blockingReason;
        parent::__construct();
    }

    /**
     * @return int
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getActivationDate(): ?\DateTimeInterface
    {
        return $this->activationDate;
    }

    /**
     * @return null|string
     */
    public function getBlockingReason(): ?string
    {
        return $this->blockingReason;
    }
}
