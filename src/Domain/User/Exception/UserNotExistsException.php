<?php
declare(strict_types=1);

namespace Grifix\Acl\Domain\User\Exception;

/**
 * Class UserNotFoundException
 * @package Grifix\Acl\Infrastructure\User\Exception
 */
class UserNotExistsException extends \Exception
{
    protected $propertyName;

    protected $propertyValue;

    /**
     * UserNotFoundException constructor.
     * @param string $propertyName
     * @param $propertyValue
     */
    public function __construct(string $propertyName, $propertyValue)
    {
        $this->propertyName = $propertyName;
        $this->propertyValue = $propertyValue;

        parent::__construct(sprintf('Users witch "%s" equals "%s" not exists!', $propertyName, $propertyValue));
    }
}
