<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Acl\Domain\User\Exception;

/**
 * Class EmailAlreadyConfirmedException
 *
 * @category Grifix
 * @package  Grifix\AclService\Domain\User
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class EmailIsNotConfirmedException extends \DomainException
{
    protected $userId;
    
    /**
     * EmailAlreadyConfirmedException constructor.
     *
     * @param string $email
     */
    public function __construct(string $email)
    {
        $this->userId = $email;
        $this->message = 'Email is not confirmed!';
        parent::__construct();
    }
}