<?php
declare(strict_types=1);

namespace Grifix\Acl\Domain\User\Exception;

/**
 * Class CantAssignRoleException
 * @package Grifix\Acl\Domain\User\Exception
 */
class CantAssignRoleException extends \DomainException
{
    protected $roleId;

    /**
     * CantAssignRoleException constructor.
     * @param string $roleId
     */
    public function __construct(string $roleId)
    {
        parent::__construct(sprintf('Cannot assign role with id "%s", role not exists!'));
        $this->roleId = $roleId;
    }
}