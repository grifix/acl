<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Domain\User;

use Grifix\Kit\Type\EmailInterface;

/**
 * Class UserFactory
 *
 * @category Grifix
 * @package  Grifix\AclService\Infrastructure\User
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface UserFactoryInterface
{
    /**
     * @param string $id
     * @param EmailInterface $email
     * @param string|null $password
     * @param bool $emailConfirmed
     *
     * @return UserInterface
     */
    public function createUser(
        string $id,
        EmailInterface $email,
        string $password = null,
        bool $emailConfirmed = false
    ): UserInterface;
}