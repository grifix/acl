<?php
declare(strict_types=1);

namespace Grifix\Acl\Domain\User\Password;

use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;

/**
 * Class PasswordFactory
 *
 * @package Grifix\Acl\Domain\User\Password
 */
class PasswordFactory extends AbstractFactory implements PasswordFactoryInterface
{
    protected $passwordInfrastructure;

    /**
     * PasswordFactory constructor.
     *
     * @param ClassMakerInterface $classMaker
     * @param PasswordInfrastructureInterface $passwordInfrastructure
     */
    public function __construct(
        ClassMakerInterface $classMaker,
        PasswordInfrastructureInterface $passwordInfrastructure
    ) {
        parent::__construct($classMaker);
        $this->passwordInfrastructure = $passwordInfrastructure;
    }

    /**
     * @param string|null $password
     *
     * @return PasswordInterface
     */
    public function createPassword(string $password = null): PasswordInterface
    {
        /**@var $class Password*/
        $class = $this->makeClassName(Password::class);
        return new $class($this->passwordInfrastructure, $password);
    }
}
