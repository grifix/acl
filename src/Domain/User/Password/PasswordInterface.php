<?php

namespace Grifix\Acl\Domain\User\Password;

/**
 * Class Password
 * @package Grifix\Acl\Domain\User\Password
 */
interface PasswordInterface
{
    /**
     * @param string $password
     */
    public function verify(string $password);

    /**
     * @param string $oldValue
     * @param string $newValue
     * @param string $newValueAgain
     * @return Password
     */
    public function change(string $oldValue, string $newValue, string $newValueAgain);

    /**
     * @param string $value
     *
     * @return PasswordInterface
     */
    public function set(string $value): PasswordInterface;
}