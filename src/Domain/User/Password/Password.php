<?php


namespace Grifix\Acl\Domain\User\Password;

use Grifix\Acl\Domain\User\Password\Exception\PasswordsMissMathException;
use Grifix\Acl\Domain\User\Password\Exception\WrongPasswordException;

/**
 * Class Password
 *
 * @package Grifix\Acl\Domain\User\Password
 */
class Password implements PasswordInterface
{

    /**
     * @dependency
     * @var \Grifix\Acl\Domain\User\Password\PasswordInfrastructureInterface
     */
    protected $infrastructure;

    protected $value;

    /**
     * Password constructor.
     *
     * @param PasswordInfrastructureInterface $infrastructure
     * @param string                          $value
     */
    public function __construct(
        PasswordInfrastructureInterface $infrastructure,
        string $value = null
    ) {
        $this->infrastructure = $infrastructure;
        if (!$value) {
            $value = $this->infrastructure->generatePassword();
        }
        $this->infrastructure->validatePassword($value);
        $this->value = $this->infrastructure->hashPassword($value);
    }

    /**
     * @param string $password
     */
    public function verify(string $password)
    {
        /** @noinspection PhpInternalEntityUsedInspection */
        if (!$this->infrastructure->verifyPassword($password, $this->value)) {
            throw new WrongPasswordException();
        }
    }

    /**
     * @param string $oldValue
     * @param string $newValue
     * @param string $newValueAgain
     *
     * @return Password
     */
    public function change(string $oldValue, string $newValue, string $newValueAgain): PasswordInterface
    {
        $this->infrastructure->verifyPassword($oldValue, $this->value);

        if ($newValue !== $newValueAgain) {
            throw new PasswordsMissMathException();
        }
        return new static($this->infrastructure, $newValue);
    }

    /**
     * {@inheritdoc}
     */
    public function set(string $value): PasswordInterface
    {
        return new static($this->infrastructure, $value);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->value;
    }
}
