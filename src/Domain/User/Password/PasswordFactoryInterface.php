<?php
declare(strict_types=1);

namespace Grifix\Acl\Domain\User\Password;


/**
 * Class PasswordFactory
 *
 * @package Grifix\Acl\Domain\User\Password
 */
interface PasswordFactoryInterface
{
    /**
     * @param string|null $password
     *
     * @return PasswordInterface
     */
    public function createPassword(string $password = null): PasswordInterface;
}