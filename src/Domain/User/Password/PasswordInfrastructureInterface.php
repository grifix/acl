<?php


namespace Grifix\Acl\Domain\User\Password;

/**
 * Interface PasswordInfrastructureInterface
 * @package Grifix\Acl\Domain\User\Password
 */
interface PasswordInfrastructureInterface
{
    /**
     * @param string $password
     * @return bool
     */
    public function validatePassword(string $password): bool;

    /**
     * @param string $password
     * @param string $hash
     * @return bool
     */
    public function verifyPassword(string $password, string $hash): bool;

    /**
     * @param string $password
     * @return string
     */
    public function hashPassword(string $password): string;

    /**
     * @return string
     */
    public function generatePassword(): string;
}
