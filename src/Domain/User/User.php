<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Domain\User;

use Grifix\Acl\Domain\Role\RoleInterface;
use Grifix\Acl\Domain\User\Email\UserEmailInterface;
use Grifix\Acl\Domain\User\Event\BruteForceDetectedEvent;
use Grifix\Acl\Domain\User\Event\EmailConfirmedEvent;
use Grifix\Acl\Domain\User\Event\PasswordWasResetEvent;
use Grifix\Acl\Domain\User\Event\RoleAssignedEvent;
use Grifix\Acl\Domain\User\Event\RoleDebarredEvent;
use Grifix\Acl\Domain\User\Event\UserCreatedEvent;
use Grifix\Acl\Domain\User\Exception\CantAssignRoleException;
use Grifix\Acl\Domain\User\Exception\EmailIsNotConfirmedException;
use Grifix\Acl\Domain\User\Exception\UserAlreadyActiveException;
use Grifix\Acl\Domain\User\Exception\UserIsNotActiveException;
use Grifix\Acl\Domain\User\Exception\WrongTokenException;
use Grifix\Acl\Domain\User\Password\Exception\WrongPasswordException;
use Grifix\Acl\Domain\User\Password\PasswordInterface;
use Grifix\Kit\Collection\Collection;
use Grifix\Kit\Collection\CollectionInterface;
use Grifix\Kit\Type\IpAddressInterface;

/**
 * Class UserModel
 *
 * @category Grifix
 * @package  Grifix\AclService\Domain\UserModel
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class User implements UserInterface
{

    /**
     * @var string
     */
    protected $id;

    /**
     * @var \Grifix\Acl\Domain\User\Email\UserEmail
     */
    protected $email;

    /**
     * @var \Grifix\Acl\Domain\User\Password\Password
     */
    protected $password;

    /**
     * @var \DateTimeImmutable
     */
    protected $registrationDate;

    /**
     * @var \DateTimeImmutable
     */
    protected $activationDate;

    /**
     * @var \DateTimeImmutable
     */
    protected $lastSignInDate;

    /**
     * @var bool
     */
    protected $isActive = false;

    /**
     * @var int
     */
    protected $bruteForceCounter;

    /**
     * @var \Grifix\Kit\Type\IpAddress
     */
    protected $lastIp;

    /**
     * @var string
     */
    protected $timeZone;

    /**
     * @var string
     */
    protected $blockingReason;

    /**
     * @var string
     */
    protected $token;

    /**
     * @var string
     */
    protected $sessionId;

    /**
     * @dependency
     * @var \Grifix\Acl\Domain\User\UserInfrastructureInterface
     */
    protected $infrastructure;

    /**
     * @var CollectionInterface
     */
    protected $roles;

    /**
     * User constructor.
     *
     * @param UserInfrastructureInterface $infrastructure
     * @param string $id
     * @param UserEmailInterface $email
     * @param PasswordInterface $password
     * @param string|null $timeZone
     *
     * @throws \Exception
     */
    public function __construct(
        UserInfrastructureInterface $infrastructure,
        string $id,
        UserEmailInterface $email,
        PasswordInterface $password,
        string $timeZone = null
    ) {
        $this->infrastructure = $infrastructure;
        $this->id = $id;
        $this->email = $email;
        $this->roles = new Collection();
        $this->password = $password;

        if (is_null($timeZone)) {
            /** @noinspection PhpInternalEntityUsedInspection */
            $timeZone = $this->infrastructure->getDefaultTimezone();
        }

        $this->token = uniqid();
        $this->registrationDate = new \DateTimeImmutable();
        $this->isActive = true;
        $this->bruteForceCounter = 0;
        $this->timeZone = $timeZone;
        $this->assignRole(RoleInterface::USER_ID);
        $this->infrastructure->publishEvent(new UserCreatedEvent($this->id, $this->token));
    }

    /**
     * @return void
     * @throws UserAlreadyActiveException
     */
    public function activate(): void
    {
        if ($this->isActive) {
            throw new UserAlreadyActiveException($this->id);
        }
        $this->isActive = true;
        $this->activationDate = null;
    }

    /**
     * {@inheritdoc}
     */
    public function setPassword(string $password): void
    {
        $this->password = $this->password->set($password);
    }

    /**
     * {@inheritdoc}
     */
    public function changePassword(string $oldPassword, string $newPassword, string $newPasswordAgain): void
    {
        $this->password = $this->password->change($oldPassword, $newPassword, $newPasswordAgain);
    }

    /**
     * {@inheritdoc}
     */
    public function confirmEmail(string $token): void
    {
        if ($token != $this->token) {
            throw new WrongTokenException($this->id, $token);
        }
        $this->email = $this->email->confirm();
        $password = $this->infrastructure->generatePassword();
        $this->setPassword($password);
        $this->infrastructure->publishEvent(new EmailConfirmedEvent(
            $this->id,
            strval($this->email),
            $token,
            $password
        ));
    }

    /**
     * @return void
     * @throws EmailIsNotConfirmedException
     */
    protected function isEmailConfirmedOrFail()
    {
        if (!$this->email->isConfirmed()) {
            throw new EmailIsNotConfirmedException($this->id);
        }
    }

    /**
     * @return void
     * @throws UserIsNotActiveException
     */
    protected function isActiveOrFail()
    {
        if (!$this->isActive) {
            throw new UserIsNotActiveException($this->id, $this->activationDate);
        }
    }

    /**
     * @return bool
     */
    protected function isSignedIn()
    {
        return $this->sessionId != null;
    }

    /**
     * {@inheritdoc}
     */
    public function signOut(): void
    {
        $this->sessionId = null;
    }

    /**
     * @param string $password
     * @param IpAddressInterface $ip
     * @param string $sessionId
     *
     * @return void
     * @throws EmailIsNotConfirmedException
     * @throws UserAlreadyActiveException
     * @throws UserIsNotActiveException
     * @throws WrongPasswordException
     */
    public function signIn(string $password, IpAddressInterface $ip, string $sessionId): void
    {
        $this->isEmailConfirmedOrFail();

        try {
            $this->password->verify($password);
        } catch (WrongPasswordException $e) {
            $this->bruteForceCounter++;
            if ($this->signInLimitExceed()) {
                $this->blockFor($this->infrastructure->getBruteForceBlockInterval(), self::REASON_BRUTEFORCE_ATTACK);
                $this->bruteForceCounter = 0;
                /** @noinspection PhpInternalEntityUsedInspection */
                $this->infrastructure->publishEvent(new BruteForceDetectedEvent($this->id));
                throw new UserIsNotActiveException($this->id, $this->activationDate, $this->blockingReason);
            }
            throw $e;
        }

        if (!$this->isActive && $this->canBeActive()) {
            $this->activate();
        }

        $this->isActiveOrFail();

        if ($this->isSignedIn()) {
            $this->signOut();
        }

        $this->bruteForceCounter = 0;
        $this->lastSignInDate = new \DateTime();
        $this->lastIp = $ip;
        $this->sessionId = $sessionId;
    }

    /**
     * @return bool
     */
    protected function signInLimitExceed(): bool
    {
        return $this->bruteForceCounter > $this->infrastructure->getMaxSignInAttempts();
    }

    /**
     * {@inheritdoc}
     */
    public function blockFor(\DateInterval $interval, string $reason = null): void
    {
        $this->isActive = false;
        $this->activationDate = (new \DateTime())->add($interval);
        $this->blockingReason = $reason;
    }

    /**
     * @return bool
     */
    protected function canBeActive(): bool
    {
        return $this->activationDate && $this->activationDate <= (new \DateTime());
    }

    /**
     * {@inheritdoc}
     */
    public function blockUntil(\DateTimeInterface $date, string $reason = null): void
    {
        $this->isActive = false;
        $this->activationDate = $date;
        $this->blockingReason = $reason;
    }

    /**
     * {@inheritdoc}
     */
    public function block(string $reason = null): void
    {
        $this->isActive = false;
        $this->activationDate = null;
        $this->blockingReason = $reason;
    }

    /**
     * {@inheritdoc}
     */
    public function unBlock(): void
    {
        $this->isActive = true;
        $this->activationDate = null;
    }

    /**
     * {@inheritdoc}
     */
    public function resetPassword(): void
    {
        $newPassword = $this->infrastructure->generatePassword();
        $this->password = $this->password->set($newPassword);
        $this->infrastructure->publishEvent(new PasswordWasResetEvent($newPassword, $this->id, strval($this->email)));
    }

    /**
     * {@inheritdoc}
     */
    public function assignRole(string $roleId): void
    {
        if (!$this->infrastructure->roleExists($roleId)) {
            throw new CantAssignRoleException($roleId);
        }
        $this->roles->add($roleId);
        $this->infrastructure->publishEvent(new RoleAssignedEvent($this->id, $roleId));
    }

    /**
     * {@inheritdoc}
     */
    public function debarRole(string $roleId): void
    {
        $this->roles->remove($roleId);
        $this->infrastructure->publishEvent(new RoleDebarredEvent($this->id, $roleId));
    }
}
