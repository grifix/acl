<?php
declare(strict_types=1);

namespace Grifix\Acl\Domain\User\Email;


use Grifix\Kit\Type\EmailInterface;

/**
 * Class UserEmailFactory
 * @package Grifix\Acl\Domain\User\Email
 */
interface UserEmailFactoryInterface
{
    /**
     * @param EmailInterface $email
     * @param bool $confirmed
     * @return UserEmailInterface
     */
    public function createEmail(EmailInterface $email, bool $confirmed = false): UserEmailInterface;
}