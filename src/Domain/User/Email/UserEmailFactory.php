<?php
declare(strict_types=1);

namespace Grifix\Acl\Domain\User\Email;


use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Type\EmailInterface;

/**
 * Class UserEmailFactory
 * @package Grifix\Acl\Domain\User\Email
 */
class UserEmailFactory extends AbstractFactory implements UserEmailFactoryInterface
{

    /**
     * @var UserEmailInfrastructureInterface
     */
    protected $emailInfrastructure;

    /**
     * UserEmailFactory constructor.
     * @param ClassMakerInterface $classMaker
     * @param UserEmailInfrastructureInterface $emailInfrastructure
     */
    public function __construct(ClassMakerInterface $classMaker, UserEmailInfrastructureInterface $emailInfrastructure)
    {
        parent::__construct($classMaker);
        $this->emailInfrastructure = $emailInfrastructure;
    }

    /**
     * @param EmailInterface $email
     * @param bool $confirmed
     * @return UserEmailInterface
     */
    public function createEmail(EmailInterface $email, bool $confirmed = false): UserEmailInterface
    {
        $class = $this->makeClassName(UserEmail::class);
        return new $class($this->emailInfrastructure, $email, $confirmed);
    }
}
