<?php
declare(strict_types=1);

namespace Grifix\Acl\Domain\User\Email;

/**
 * Interface EmailInfrastructureInterface
 * @package Grifix\Acl\Domain\User\Email
 */
interface UserEmailInfrastructureInterface
{
    /**
     * @param $email
     * @return bool
     */
    public function emailExists($email): bool;
}
