<?php
declare(strict_types=1);

namespace Grifix\Acl\Domain\User\Email;

use Grifix\Acl\Domain\User\Email\Exception\UserEmailAlreadyConfirmedException;
use Grifix\Acl\Domain\User\Email\Exception\UserEmailAlreadyExistsException;
use Grifix\Kit\Type\EmailInterface;

/**
 * Class Email
 * @package Grifix\Acl\Domain\User\Email
 */
class UserEmail implements UserEmailInterface
{

    /**
     * @var \Grifix\Kit\Type\Email
     */
    private $email;

    /**
     * @var bool
     */
    private $confirmed;

    /**
     * @dependency
     * @var \Grifix\Acl\Domain\User\Email\UserEmailInfrastructureInterface
     */
    private $infrastructure;

    /**
     * Email constructor.
     * @param UserEmailInfrastructureInterface $infrastructure
     * @param EmailInterface $email
     * @param bool $confirmed
     */
    public function __construct(
        UserEmailInfrastructureInterface $infrastructure,
        EmailInterface $email,
        bool $confirmed = false
    ) {
        $this->infrastructure = $infrastructure;
        if ($this->infrastructure->emailExists(strval($email))) {
            throw new UserEmailAlreadyExistsException(strval($email));
        }
        $this->email = $email;
        $this->confirmed = $confirmed;
    }


    /**
     * {@inheritdoc}
     */
    public function confirm(): UserEmailInterface
    {
        if ($this->confirmed) {
            throw new UserEmailAlreadyConfirmedException(strval($this->email));
        }
        $result = clone $this;
        $result->confirmed = true;
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function isConfirmed(): bool
    {
        return $this->confirmed;
    }

    public function __toString(): string
    {
        return strval($this->email);
    }
}
