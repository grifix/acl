<?php
declare(strict_types=1);

namespace Grifix\Acl\Domain\User\Email;

/**
 * Class Email
 * @package Grifix\Acl\Domain\User\Email
 */
interface UserEmailInterface
{
    /**
     * @return UserEmailInterface
     */
    public function confirm(): UserEmailInterface;

    /**
     * @return bool
     */
    public function isConfirmed(): bool;
}