<?php
declare(strict_types=1);

namespace Grifix\Acl\Domain\User\Email\Exception;

/**
 * Class UserEmailAlreadyExists
 * @package Grifix\Acl\Domain\User\Email\Exception
 */
class UserEmailAlreadyExistsException extends \DomainException
{
    protected $email;

    /**
     * UserEmailAlreadyExists constructor.
     * @param string $email
     */
    public function __construct(string $email)
    {
        parent::__construct(sprintf('User with email %s already exists!', $email));
        $this->email = $email;
    }
}