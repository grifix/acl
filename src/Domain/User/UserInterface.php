<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Domain\User;

use Grifix\Acl\Domain\Role\RoleInterface;
use Grifix\Acl\Domain\User\Email\Exception\UserEmailAlreadyConfirmedException;
use Grifix\Acl\Domain\User\Exception\CantAssignRoleException;
use Grifix\Acl\Domain\User\Exception\EmailIsNotConfirmedException;
use Grifix\Acl\Domain\User\Exception\PasswordsMissMathException;
use Grifix\Acl\Domain\User\Exception\UserIsNotActiveException;
use Grifix\Acl\Domain\User\Exception\WeakPasswordException;
use Grifix\Acl\Domain\User\Exception\WrongPasswordException;
use Grifix\Acl\Domain\User\Exception\WrongTokenException;
use Grifix\Kit\Type\IpAddressInterface;


/**
 * Interface UserInterface
 *
 * @category Grifix
 * @package  Grifix\AclService\Domain\User
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface UserInterface
{
    const GUEST_ID = '1';
    const ADMIN_ID = '2';
    const REASON_BRUTEFORCE_ATTACK = 'brute_force_attack';

    public function activate();

    /**
     * @param string $token
     *
     * @return void
     * @throws UserEmailAlreadyConfirmedException
     * @throws WrongTokenException
     */
    public function confirmEmail(string $token): void;

    /**
     * @param string $oldPassword
     * @param string $newPassword
     * @param string $newPasswordAgain
     *
     * @return void
     * @throws PasswordsMissMathException
     */
    public function changePassword(string $oldPassword, string $newPassword, string $newPasswordAgain): void;

    /**
     * @param string $password
     * @param IpAddressInterface $ip
     * @param string $sessionId
     *
     * @return void
     * @throws EmailIsNotConfirmedException
     * @throws UserIsNotActiveException
     * @throws WrongPasswordException
     */
    public function signIn(string $password, IpAddressInterface $ip, string $sessionId): void;

    /**
     * @return void
     */
    public function signOut(): void;

    /**
     * @return void
     */
    public function resetPassword(): void;

    /**
     * @param \DateInterval $interval
     * @param string|null $reason
     *
     * @return void
     */
    public function blockFor(\DateInterval $interval, string $reason = null): void;

    /**
     * @param \DateTimeInterface $date
     * @param string|null $reason
     *
     * @return void
     */
    public function blockUntil(\DateTimeInterface $date, string $reason = null): void;

    /**
     * @param string|null $reason
     *
     * @return void
     */
    public function block(string $reason = null): void;

    /**
     * @return void
     */
    public function unBlock(): void;

    /**
     * @param string $roleId
     *
     * @throws CantAssignRoleException
     */
    public function assignRole(string $roleId): void;

    /**
     * @param int $roleId
     */
    public function debarRole(string $roleId): void;

    /**
     * @param string $password
     *
     * @return void
     * @throws WeakPasswordException
     */
    public function setPassword(string $password): void;


}