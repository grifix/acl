<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Acl\Domain\User;

use Grifix\Acl\Infrastructure\User\UserInfrastructure;
use Grifix\Kit\Orm\EventCollector\EventPublisherInterface;

/**
 * Class UserInfrastructure
 *
 * @category Grifix
 * @package  Grifix\AclService\Infrastructure\User
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface UserInfrastructureInterface extends EventPublisherInterface
{
    /**
     * @param string $email
     * @param string $token
     *
     * @return void
     */
    public function sendConfirmationMessage(string $email, string $token): void;
    
    /**
     * @internal
     *
     * @return string
     */
    public function getDefaultTimezone(): string;
    
    /**
     * @return int
     */
    public function getMaxSignInAttempts();
    
    /**
     * @return \DateInterval
     */
    public function getBruteForceBlockInterval(): \DateInterval;

    /**
     * @return string
     */
    public function generatePassword():string;

    /**
     * @param string $roleId
     * @return bool
     */
    public function roleExists(string $roleId): bool;
}