<?php
declare(strict_types=1);

namespace Grifix\Acl\Domain\Role;

use Grifix\Acl\Domain\Role\Exception\RoleNotExistsException;
use Grifix\Acl\Infrastructure\Role\RoleRepository;
use Grifix\Kit\Collection\GenericCollectionInterface;

/**
 * Interface RoleRepositoryInterface
 * @package Grifix\Acl\Domain\Role
 */
interface RoleRepositoryInterface extends GenericCollectionInterface
{
    /**
     * @param string $id
     * @throws RoleNotExistsException
     * @return RoleInterface
     */
    public function find(string $id): RoleInterface;

    /**
     * @param RoleInterface $role
     */
    public function save(RoleInterface $role): void;

    /**
     * @param RoleInterface $role
     */
    public function delete(RoleInterface $role): void;

    /**
     * @param string $name
     * @return RoleInterface
     * @throws RoleNotExistsException
     */
    public function findByName(string $name): RoleInterface;

    /**
     * @param string $id
     * @return bool
     */
    public function exists(string $id): bool;
}
