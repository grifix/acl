<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Acl\Domain\Role;

/**
 * Class Role
 *
 * @category Grifix
 * @package  Grifix\AclService\Role
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface RoleInterface
{
    const GUEST_ID = '1';
    const USER_ID = '2';
    const ADMIN_ID = '3';
    
    /**
     * @param string $permission
     *
     * @return void
     */
    public function grantPermission(string $permission): void;

    
    /**
     * @param string $permission
     *
     * @return void
     */
    public function denyPermission(string $permission): void;

    /**
     * @return void
     */
    public function clearPermissions(): void;

}
