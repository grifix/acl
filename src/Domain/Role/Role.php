<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Domain\Role;

use Grifix\Kit\Collection\Collection;
use Grifix\Kit\Collection\CollectionInterface;

/**
 * Class Role
 *
 * @category Grifix
 * @package  Grifix\AclService\Role
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Role implements RoleInterface
{

    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var CollectionInterface
     */
    protected $permissions;

    /**
     * @dependency
     * @var \Grifix\Acl\Domain\Role\RoleInfrastructureInterface
     */
    protected $infrastructure;

    /**
     * Role constructor.
     *
     * @param RoleInfrastructureInterface $infrastructure @dependency
     * @param string $id
     * @param string $name
     */
    public function __construct(
        RoleInfrastructureInterface $infrastructure,
        string $id,
        string $name
    ) {
        $this->id = $id;
        $this->infrastructure = $infrastructure;
        $this->name = $name;
        $this->permissions = new Collection();
    }

    /**
     * {@inheritdoc}
     */
    public function grantPermission(string $permission): void
    {
        $this->permissions->add($permission);
    }

    /**
     * {@inheritdoc}
     */
    public function denyPermission(string $permission): void
    {
        $this->permissions->remove($permission);
    }

    /**
     * {@inheritdoc}
     */
    public function clearPermissions(): void
    {
        $this->permissions->clear();
    }
}
