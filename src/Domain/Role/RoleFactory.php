<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Acl\Domain\Role;

use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;

/**
 * Class RoleFactory
 *
 * @category Grifix
 * @package  Grifix\AclService\Domain\Role
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class RoleFactory extends AbstractFactory implements RoleFactoryInterface
{
    
    /**
     * @var RoleInfrastructureInterface
     */
    protected $roleInfrastructure;
    /**
     * RoleFactory constructor.
     *
     * @param ClassMakerInterface         $classMaker
     * @param RoleInfrastructureInterface $roleInfrastructure
     */
    public function __construct(
        ClassMakerInterface $classMaker,
        RoleInfrastructureInterface $roleInfrastructure
    ) {
        $this->roleInfrastructure = $roleInfrastructure;
        parent::__construct($classMaker);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createRole(string $id, string $name): RoleInterface
    {
        $class = $this->makeClassName(Role::class);
        
        $role = new $class($this->roleInfrastructure, $id, $name);
        
        return $role;
    }
}
