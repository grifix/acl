<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace {

    use Grifix\Acl\Infrastructure\Permission;

    return [
        'root' => [
            'pattern' => '/{locale:locale?}/grifix/acl',
            'children' => [
                'admin' => [
                    'pattern' => '/admin',
                    'children' => [
                        'users' => [
                            'pattern' => '/users',
                            'handler' => 'grifix.acl.admin.users',
                        ],
                        'roles' => [
                            'pattern' => '/roles',
                            'handler' => 'grifix.acl.admin.roles',
                        ],
                    ]
                ],
                'signIn' => [
                    'pattern' => '/signIn',
                    'handler' => 'grifix.acl.signIn',
                ],

            ],
        ],
    ];
}
