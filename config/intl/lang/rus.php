<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace {
    
    return [
        '_module' => 'ACL',
        'userNotExists' => 'Такого пользователя не существует!',
        'userIsBlockedUntil' => 'Пользователь заблокирован до {date}!',
        'userIsBlocked' => 'Пользоватль заблокирован!',
        'signIn' => 'Войти',
        'signOut' => 'Выйти',
        'user' => [
            'cases' => [
                'nom' => ['пользователь', 'пользователи'],
            ],
        ],
        'role' => [
            'cases' => [
                'nom' => ['роль', 'роли'],
            ],
        ],
    ];
}
