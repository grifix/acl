<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace {
    
    return [
        '_module' => 'ACL',
        'userNotExists' => 'This user does not exist!',
        'userIsBlockedUntil' => 'UserModel is blocked until {date}!',
        'userIsBlocked' => 'UserModel is blocked!',
        'signIn' => 'Sign in',
        'signOut' => 'Sign out',
        'user' => [
            'cases' => [
                'nom' => ['user', 'users'],
            ],
        ],
        'role' => [
            'cases' => [
                'nom' => ['role', 'roles'],
            ],
        ],
    ];
}
