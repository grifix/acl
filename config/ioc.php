<?php


namespace {

    use Grifix\Acl\Application\Contract\RelationsManagerInterface;
    use Grifix\Acl\Application\Contract\RoleFinder\RoleFinderInterface;
    use Grifix\Acl\Application\Contract\UserFinder\UserFinderInterface;
    use Grifix\Acl\Domain\Role\RoleInfrastructureInterface;
    use Grifix\Acl\Domain\User\Email\UserEmailInfrastructureInterface;
    use Grifix\Acl\Domain\User\Password\PasswordInfrastructureInterface;
    use Grifix\Acl\Domain\User\UserInfrastructureInterface;
    use Grifix\Acl\Infrastructure\Role\RoleFinder;
    use Grifix\Acl\Infrastructure\Role\RoleInfrastructure;
    use Grifix\Acl\Infrastructure\Role\RelationsManager;
    use Grifix\Acl\Infrastructure\User\Email\UserUserEmailInfrastructure;
    use Grifix\Acl\Infrastructure\User\Password\PasswordInfrastructure;
    use Grifix\Acl\Infrastructure\User\UserFinder;
    use Grifix\Acl\Infrastructure\User\UserInfrastructure;
    use Grifix\Kit\Ioc\Definition;

    return [
        UserInfrastructureInterface::class => new Definition(UserInfrastructure::class),
        UserEmailInfrastructureInterface::class => new Definition(UserUserEmailInfrastructure::class),
        PasswordInfrastructureInterface::class => new Definition(PasswordInfrastructure::class),
        RoleInfrastructureInterface::class => new Definition(RoleInfrastructure::class),
        RelationsManagerInterface::class => new Definition(RelationsManager::class),
        RoleFinderInterface::class => new Definition(RoleFinder::class),
        UserFinderInterface::class => new Definition(UserFinder::class)
    ];
}
