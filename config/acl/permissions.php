<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace {

    use Grifix\Acl\Ui\AclModulePermissions as P;
    use Grifix\Acl\Ui\Permission;

    return [
        P::SIGN_IN => new Permission(
            P::SIGN_IN,
            ['guest']
        ),
        P::SIGN_UP => new Permission(
            P::SIGN_UP,
            ['guest']
        ),
        P::CHANGE_PASSWORD => new Permission(
            P::CHANGE_PASSWORD,
            ['user']
        ),
        P::CONFIRM_EMAIL => new Permission(
            P::CONFIRM_EMAIL,
            ['guest']
        ),
        P::RESET_PERMISSION => new Permission(
            P::RESET_PERMISSION,
            ['admin']
        ),
        P::GET_SIGNED_IN_USER => new Permission(
            P::GET_SIGNED_IN_USER,
            ['user', 'guest', 'admin']
        ),
        P::ADMIN_USERS => new Permission(
            P::ADMIN_USERS,
            ['admin']
        ),
        P::READ_USER_TIMEZONE => new Permission(
            P::READ_USER_TIMEZONE,
            ['admin']
        ),
        P::READ_USER_TOKEN => new Permission(
            P::READ_USER_TOKEN,
            ['admin']
        ),
        P::READ_USER_LAST_IP => new Permission(
            P::READ_USER_LAST_IP,
            ['admin']
        ),
        P::READ_USER_BRUTEFORCE_COUNTER => new Permission(
            P::READ_USER_BRUTEFORCE_COUNTER,
            ['admin']
        ),
        P::READ_USER_PERMISSIONS => new Permission(
            P::READ_USER_PERMISSIONS,
            ['admin']
        ),
        P::READ_USER_EMAIL => new Permission(
            P::READ_USER_EMAIL,
            ['admin']
        )
    ];
}
