<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace {

    use Grifix\Acl\Ui\AclModulePermissions;

    return [
        'grifix.acl.signUp' => AclModulePermissions::SIGN_UP,
        'grifix.acl.confirmEmail' => AclModulePermissions::CONFIRM_EMAIL,
        'grifix.acl.signIn' => AclModulePermissions::SIGN_IN,
        'grifix.acl.changePassword' => AclModulePermissions::CHANGE_PASSWORD,
        'grifix.acl.resetPermission' => AclModulePermissions::RESET_PERMISSION,
        'grifix.acl.getSignedInUser' => AclModulePermissions::GET_SIGNED_IN_USER
    ];
}
