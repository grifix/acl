<?php declare(strict_types=1);

namespace {

    use Grifix\Acl\Application\Query\FindUsers\FindUsersQueryResult;
    use Grifix\Kit\Helper\ArrayHelperInterface;

    /**@var $this \Grifix\Kit\View\ViewInterface */

    /**@var FindUsersQueryResult $result */
    $result = $this->getVar('result');
    $json = [
        'total' => $result->getTotal(),
        'rows' => []
    ];
    foreach ($result->getUsers() as $user) {
        $json['rows'][] = $this->getShared(ArrayHelperInterface::class)->toArray($user);
    }

    echo json_encode($json);

}
