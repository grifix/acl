<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
/**@var $this \Grifix\Kit\View\ViewInterface */
?>
<form class="panel <?= $this->getCssClass() ?> <?= $this->getWidgetClass() ?>"
      data-redirect="<?= $this->getTextVar('redirect', '/') ?>">
    <div class="panel-body">
        <?= $this->getHelper()->createEmailInputBuilder()
            ->setName('email')
            ->setPlaceholder($this->translate('grifix.kit.email'))
            ->setRequired()
            ->build() ?>
        <?= $this->getHelper()->createPasswordInputBuilder()
            ->setName('password')
            ->setPlaceholder($this->translate('grifix.kit.password'))
            ->setRequired()
            ->build()
        ?>
        <button data-role="submit" class="button"><?= $this->translate('grifix.acl.signIn') ?></button>
    </div>
</form>
