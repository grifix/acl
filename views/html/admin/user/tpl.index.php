<?php
declare(strict_types=1);
/**@var $this Grifix\Kit\View\ViewInterface */
$this->inherits('grifix.admin.{skin}.lyt.default');
$this->addJs('{src}/grifix/acl/views/{skin}/admin/user/Grid.js');
?>
<?php $this->startBlock('title')?>
    <?=$this->translate('grifix.kit.user.p')?> - <?=$this->getParentBlock()?>
<?php $this->endBlock()?>

<?php $this->startBlock('content') ?>
    <?= $this->renderPartial('grifix.acl.{skin}.admin.user.prt.grid') ?>
<?php $this->endBlock() ?>
