/*require {src}/grifix/kit/views/{skin}/Widget.js*/
/*require {src}/grifix/admin/views/{skin}/Grid.js*/
jQuery(function ($) {
    "use strict";

    gfx.createWidgetPrototype(
        gfx.createConstructorName(),
        gfx.createWidgetName(),
        grifix.admin.Grid,
        {
            api: {
                load: gfx.routeToAction('grifix.acl.getUsers','grifix.acl.json.user.prt.grid')
            },
            columns:[
                {
                    field: 'id',
                    title: php(/*$translate('grifix.kit.id')*/)
                },
                {
                    field: 'email',
                    title: php(/*$translate('grifix.kit.email')*/)
                }
            ]
        },
        {
            init: function () {
                var that = this;

                that.callParent('init');

            }

        }
    );

});
