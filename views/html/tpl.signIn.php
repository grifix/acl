<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

/**@var $this \Grifix\Kit\View\ViewInterface*/
$this->inherits('grifix.kit.{skin}.lyt.default');
$this->addJs('{src}/grifix/acl/views/{skin}/SignIn.js');
$this->addCss('{src}/grifix/acl/views/{skin}/tpl.signIn.css');
?>

<?php $this->startBlock('content')?>
<div class="<?=$this->getCssClass()?>">
    <?= $this->renderPartial('grifix.acl.{skin}.prt.signIn')?>
</div>
<?php $this->endBlock()?>


